#include <GL/glut.h>
#include <GL/glu.h>
#include <GL/gl.h>
#include <iostream>
#include <cmath>
#include <climits>

GLfloat rotx=0.0;
GLfloat roty=0.0;
GLfloat light0_position[] = { 1.0, 1.0, 1.0, 1.0 }; // 1 posizionale, 0 direzionale

GLfloat cadutaPetali[24];
GLfloat rotazionePetali[24];

int numPetali = 9;

#define COLLINA 1
#define GAMBO 2
#define PETALO 3

double toRadian(float gradi){
   return gradi * M_PI / 180;
}

float parabola(float x, float z){
  float alfa = - 0.002;
  return (pow(x,2) + pow(z,2))*alfa;
}


void init (void) {

  for (int i = 0; i < 24;i++)
    cadutaPetali[i] = 0;

  GLfloat light0_ambient[] = { 0.3, 0.3, 0.3, 1.0 };
  GLfloat light0_diffuse[] = { 0.4, 0.4, 0.4, 1.0 };
  GLfloat light0_specular[] = { 1.0, 1.0, 1.0, 1.0 };
  GLfloat light0_position[] = { 1.0, 1.0, 1.0, 0.0 };
  GLfloat  specref[] =  { 0.6f, 0.6f, 0.6f, 1.0f };
  GLfloat  ambientLight[] = { 0.5f, 0.5f, 0.5f, 1.0f};

   glLightModelfv(GL_LIGHT_MODEL_AMBIENT, ambientLight);

   glLightfv (GL_LIGHT0, GL_AMBIENT, light0_ambient);
   glLightfv (GL_LIGHT0, GL_DIFFUSE, light0_diffuse);
   glLightfv (GL_LIGHT0, GL_SPECULAR, light0_specular);
   glLightfv (GL_LIGHT0, GL_POSITION, light0_position);
   glEnable(GL_LIGHT0);
   
   glEnable(GL_COLOR_MATERIAL);
   glColorMaterial(GL_FRONT, GL_AMBIENT_AND_DIFFUSE);
    glMaterialfv(GL_FRONT, GL_SPECULAR,specref);
   glMateriali(GL_FRONT, GL_SHININESS,128);
   
   glEnable (GL_LIGHTING);
   glEnable (GL_LIGHT0);
   //glEnable(GL_CULL_FACE);
   glEnable(GL_DEPTH_TEST);   
   glShadeModel(GL_SMOOTH);

   glNewList(COLLINA,GL_COMPILE);
    glColor3f(0,0.8,0);
      for (float x = -50; x < 50; x = x + 0.5){
        glBegin(GL_QUAD_STRIP);
        for (float z = -50; z < 50; z = z + 0.5){
            glVertex3f(x,parabola(x,z),z);
            glVertex3f(x,parabola(x,z+0.5),z+0.5);
            glVertex3f(x+0.5,parabola(x+0.5,z),z);
            glVertex3f(x+0.5,parabola(x+0.5,z+0.5),z+0.5);
        }
        glEnd();
      }
   glEndList();


    glNewList(GAMBO,GL_COMPILE);
    glColor3f(0,0.7,0);
      glBegin(GL_TRIANGLE_FAN);
      glVertex3f(0,0,0);
      for (float angle = 0; angle <= 360; angle = angle + 0.5){
          glVertex3f(cos(toRadian(angle))*2,0,sin(toRadian(angle))*2);
      }
      glEnd();

      glBegin(GL_TRIANGLE_FAN);
      glVertex3f(0,30,0);
      for (float angle = 0; angle <= 360; angle = angle + 0.5){
          glVertex3f(cos(toRadian(angle))*1.5,60,sin(toRadian(angle))*1.5);
      }
      glEnd();

      glBegin(GL_QUAD_STRIP);
      for (float angle = 0; angle <= 360; angle = angle + 0.5){
        glVertex3f(cos(toRadian(angle))*1.5,60,sin(toRadian(angle))*1.5);
          glVertex3f(cos(toRadian(angle))*2,0,sin(toRadian(angle))*2);
      }
      glEnd();
   glEndList();


   glNewList(PETALO,GL_COMPILE);
      glPushMatrix();
        glColor3f(1,1,1);
        glScalef(3,1.3 ,0.7);
        glutSolidSphere(4,50,50);
      glPopMatrix();
   glEndList();


   glEnable(GL_NORMALIZE);

}


  void drawWorld(){

      glCallList(COLLINA);

  glPushMatrix();
    glCallList(GAMBO);
    glTranslatef(0,60,0);
    glTranslatef(0,parabola(5,0),0);

    // centro fiore
    glColor3f(0.9,0.8,0);
    glScalef(1,1,0.5);
    glTranslatef(0,8,0);
    glutSolidSphere(8,50,50);

    glInitNames();
    glPushName(0);
    // petali
    glPushMatrix();
      for (int pet = 1; pet <= numPetali; pet++){
          glPushMatrix();
            glLoadName(pet-1);      
            if (cadutaPetali[pet-1] != -60) {
              glTranslatef(0,cadutaPetali[pet-1],0);
              glRotatef((360/numPetali)*pet,0,0,1);
              glRotatef(10,0,1,0);
            }
            else {

            }
            glTranslatef(-18,0,2);
            glCallList(PETALO);
          glPopMatrix();
      }
    glPopName();
    glPopMatrix();

    // foglia
    glPushMatrix();
        glColor3f(0,0.8,0);
        glTranslatef(0,-40,0);     
        glRotatef(20,0,0,1);
        glTranslatef(-2,+8,-0.2);
        glScalef(0.3,0.8,0.1);
        glutSolidSphere(16,50,50);
    glPopMatrix();



  glPopMatrix();
}

void display (void) {
  glClearColor(0,0.8,0.8,1);
   glClear (GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
   glPushMatrix();
   glTranslatef(0,-50,-150);
     glRotatef(rotx,1,0,0);
     glRotatef(roty,0,1,0);
     drawWorld();
   glPopMatrix();
   glutSwapBuffers ();
}

void reshape(int w, int h) {
   // Prevent a divide by zero
	if(h == 0)
		h = 1;

	// Set Viewport to window dimensions
    glViewport(0, 0, w, h);

	// Calculate aspect ratio of the window
	float fAspect = (GLfloat)w/(GLfloat)h;

	// Set the perspective coordinate system
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	gluPerspective(80.0, (GLfloat) w/(GLfloat) h, 1.0, 800.0);

   glMatrixMode (GL_MODELVIEW);
   glLoadIdentity ();

}

void keyboard(unsigned char key, int x, int y) {
   switch (key) {
      case 27:
         exit(0);
         break;
      case 'a':
         rotx+=5.0;
         break;
      case 's':
         rotx-=5.0;
         break;
	  case 'x':
         roty+=5.0;
         break;
      case 'z':
         roty-=5.0;
         break;
      case '+':
         numPetali++;
         if (numPetali > 24) numPetali = 24;
         break;
      case '-':
         numPetali--;
         cadutaPetali[numPetali] = 0;
         if (numPetali < 1) numPetali = 1;
         break;
   }
   glutPostRedisplay();
}


void Process(GLuint *pSelectBuff){
  int ogg = pSelectBuff[3];

  std::cout << pSelectBuff[0] << " -- " << pSelectBuff[1] << " -- " << pSelectBuff[2] << " -- " << pSelectBuff[3] << " -- " << std::endl;
  if (cadutaPetali[ogg] > -60)
      cadutaPetali[ogg] = cadutaPetali[ogg] - 2;
  std::cout << "Caduta petalo "<< ogg << " di" << cadutaPetali[ogg] << std::endl;
  /*if (ogg == SCENA){
    animated = !animated;
    std::cout<< "click" << animated << std::endl;
  }*/
  
}


void process_zObject(GLuint *selectBuffer, GLint hits) {
  int id = 0;
  GLuint z_min = UINT_MAX; // Sparo al massimo la z
  GLuint id_min = INT_MIN; // Sparo al minimo l'id

  for(int i=0; i<hits; i++) {
    for(int j=id; j<=(id+2+selectBuffer[id]); j++) {
      std::cout << selectBuffer[j] << std::endl;
    }

    std::cout << std::endl;
    // Se il valore di z nel buffer
    if(selectBuffer[id+1] < z_min) {
      // Prendere il valore della z_min nel buffer
      z_min = selectBuffer[id+1];
      id_min = selectBuffer[id+2+selectBuffer[id]];
    }

    id += selectBuffer[id]+3;
  }

  std::cout << "Selected object: " << id_min << std::endl;

  glutPostRedisplay();
}

#define BUFFER_LENGTH 64
void processSelection(int xPos, int yPos){
  GLfloat fAspect;  // Screen aspect ratio

  // Space for selection buffer
  GLuint selectBuff[BUFFER_LENGTH];

  // Hit counter and viewport storeage
  GLint viewport[4];

  // Setup selection buffer
  glSelectBuffer(BUFFER_LENGTH, selectBuff);
  
  // Get the viewport
  glGetIntegerv(GL_VIEWPORT, viewport);

  // Switch to projection and save the matrix
  glMatrixMode(GL_PROJECTION);
  glPushMatrix();

  // Change render mode
  glRenderMode(GL_SELECT);

  // Establish new clipping volume to be unit cube around
  // mouse cursor point (xPos, yPos) and extending two pixels
  // in the vertical and horzontal direction. Remember OpenGL specifies the
  // y coordinate from the bottom, Windows from the top. So windows position
  // (as measured from the top) subtract the height and you get it in terms 
  // OpenGL Likes.
  glLoadIdentity();
  gluPickMatrix(xPos, viewport[3] - yPos, 2,2, viewport);

  // Apply perspective matrix 
  fAspect = (float)viewport[2] / (float)viewport[3];

  gluPerspective(80.0f, fAspect, 1.0f, 800.0f);

  // Draw the scene
 display();
  
  GLint hits = 0;
  hits = glRenderMode(GL_RENDER);
  std::cout << "Conteggio di hits: " << hits << std::endl;
  // If a single hit occured, display the info.
  if(hits >= 1){
    // process_zObject(selectBuff,hits);
    Process(selectBuff);
  }

  // Restore the projection matrix
  glMatrixMode(GL_PROJECTION);
  glPopMatrix();

  // Go back to modelview for normal rendering
  // glMatrixMode(GL_MODELVIEW);

  // Collect the hits
  
}

void mouse( int but, int state, int x, int y ){
  if (but == GLUT_LEFT_BUTTON && state == GLUT_DOWN)
    processSelection(x,y);
}


void idleScene(){
    for (int i = 0; i <24; i++){
      if (cadutaPetali[i] != 0 && cadutaPetali[i] > -60)
        cadutaPetali[i] = cadutaPetali[i] - 2;
    }
   glutPostRedisplay();
}

int main(int argc, char** argv) {
   glutInit(&argc, argv);
   glutInitDisplayMode (GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
   glutInitWindowSize (800, 600);
   glutCreateWindow ("Fiori");
   init ();
   glutReshapeFunc (reshape);
   glutDisplayFunc(display);
   glutIdleFunc(idleScene);
   glutMouseFunc(mouse);
   glutKeyboardFunc (keyboard);
   glutMainLoop();
   return 0; 
}
