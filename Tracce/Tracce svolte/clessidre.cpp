#include <GL/glut.h>
#include <GL/glu.h>
#include <GL/gl.h>

#define GLASS1 1
#define GLASS2 2

void hour_glass() {
	glNewList(GLASS1,GL_COMPILE);
		glBegin(GL_QUADS);
			// Base Inferiore
			glColor3f(0.0f,0.3f,0.0f);
			glVertex3f(2.0,0,-2.0);
			glVertex3f(-2.0,0,-2.0);
			glVertex3f(-2.0,0,2.0);
			glVertex3f(2.0,0,2.0);
		glEnd();

		glPushMatrix();
			glColor3f(0.0f,0.3f,0.0f);
			glutSolidCube(2);
		glPopMatrix();
	glEndList();

	glCallList(GLASS1);
}

void render_scene() {
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glClearColor(0.1f,0.1f,0.1f,1.0f);

	glEnable(GL_DEPTH_TEST);

	//hour_glass();
	glPushMatrix();
			glColor3f(0.0f,0.3f,0.0f);
			glutSolidCube(1);
	glPopMatrix();

	glFlush();
	glutSwapBuffers();
}

void keyboard(unsigned char key, int x, int y) {

}

void idle() {

}

void reshape(int w,int h){
	GLfloat fAspect;
	if(h == 0)
		h = 1;
	glViewport(0,0,w,h);
	fAspect = (GLfloat)w/(GLfloat)h;
	glMatrixMode(GL_PROJECTION);
	gluPerspective(60.0f,fAspect,1.0f,400.0f);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(0.0f,-10.0f,-50.0f);
}	

int main(int argc,char** argv) {
	glutInit(&argc,argv);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA | GLUT_DEPTH);
	glutInitWindowSize(1024,800);
	glutInitWindowPosition(194,100);
	glutCreateWindow("Clessidre");
	glutReshapeFunc(reshape);
	glutDisplayFunc(render_scene);
	glutKeyboardFunc(keyboard);
	glutIdleFunc(idle);
	
	glutMainLoop();
}