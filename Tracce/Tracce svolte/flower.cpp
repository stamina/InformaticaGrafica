#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glut.h>
#include <cmath>

GLfloat ambient[4] = {0.4,0.4,0.4,0.1};
GLfloat specular[4] = {0.8,0.8,0.8,0.2};
GLfloat light_pos[4] = {50.0f, 100.0f, 5.0f, 1.0f};
GLfloat y_angle = 0;

bool y_rot = false;

void init_and_enable() {
	glClearColor(0.2f,0.0f,0.8f,1.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); 

	// Enable
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_ALPHA_TEST);
	glEnable(GL_COLOR_MATERIAL);
	glEnable(GL_LIGHTING);
	glEnable(GL_LIGHT0);

	glLightModelfv(GL_LIGHT_MODEL_AMBIENT,ambient);
	glLightfv(GL_LIGHT0,GL_DIFFUSE,specular);
	glLightfv(GL_LIGHT0,GL_POSITION,light_pos);

	glColorMaterial(GL_FRONT_AND_BACK,GL_AMBIENT_AND_DIFFUSE);
	glMateriali(GL_FRONT,GL_SHININESS,100);
	glAlphaFunc(GL_GREATER,0.5);
}

void draw_grass() {

	GLuint tex;
	glGenTextures(1,&tex);
	glShadeModel(GL_SMOOTH);
	glRotatef(y_angle,0,y_rot,0);
	glColor3f(0.0f,1.0f,0.0f);
	glPushMatrix();
		glScalef(5.0f,0.2f,5.0f);
		glutSolidCube(10);
	glPopMatrix();
}

void draw_flower1() {
	glShadeModel(GL_SMOOTH);
	glColor3f(0.0f,0.4f,0.0f);
	glPushMatrix();
		glTranslatef(-8.0f,4.0f,0.9f);
		glScalef(0.1f,2.8f,0.1f);
		glutSolidCube(8);
	glPopMatrix();

	glColor3f(0.6,0.2,0.0);
	glPushMatrix();
		glTranslatef(-8.0f,16.0f,0.9f);
		glScalef(1.0,1.0,0.5f);
		glutSolidSphere(2.5,100,100);
	glPopMatrix();

	glColor3f(0.9f,0.9f,0.9f);
	glPushMatrix();
		glTranslatef(-12.0f,16.0f,0.9f);
		glScalef(1.0f,0.5f,0.05f);
		glutSolidSphere(3,100,100);
	glPopMatrix();

	glColor3f(0.9f,0.9f,0.9f);
	glPushMatrix();
		glTranslatef(-8.0f,20.0f,0.9f);
		glScalef(0.5f,1.0f,0.05f);
		glutSolidSphere(3,100,100);
	glPopMatrix();

	glColor3f(0.9f,0.9f,0.9f);
	glPushMatrix();
		glTranslatef(-10.0f,20.0f,0.9f);
		glRotatef(20,0,0,1);
		glScalef(0.5f,1.0f,0.05f);
		glutSolidSphere(3,100,100);
	glPopMatrix();

	glColor3f(0.9f,0.9f,0.9f);
	glPushMatrix();
		glTranslatef(-11.0f,19.0f,0.9f);
		glRotatef(50,0,0,1);
		glScalef(0.5f,1.0f,0.05f);
		glutSolidSphere(3,100,100);
	glPopMatrix();

	glColor3f(0.9f,0.9f,0.9f);
	glPushMatrix();
		glTranslatef(-12.0f,17.5f,0.9f);
		glRotatef(70,0,0,1);
		glScalef(0.5f,1.0f,0.05f);
		glutSolidSphere(3,100,100);
	glPopMatrix();

	glColor3f(0.9f,0.9f,0.9f);
	glPushMatrix();
		glTranslatef(-12.0f,17.5f,0.9f);
		glRotatef(70,0,0,1);
		glScalef(0.5f,1.0f,0.05f);
		glutSolidSphere(3,100,100);
	glPopMatrix();

	glColor3f(0.9f,0.9f,0.9f);
	glPushMatrix();
		glTranslatef(-11.5f,14.0f,0.9f);
		glRotatef(105,0,0,1);
		glScalef(0.5f,1.0f,0.05f);
		glutSolidSphere(3,100,100);
	glPopMatrix();

	glColor3f(0.9f,0.9f,0.9f);
	glPushMatrix();
		glTranslatef(-10.5f,13.0f,0.9f);
		glRotatef(140,0,0,1);
		glScalef(0.5f,1.0f,0.05f);
		glutSolidSphere(3,100,100);
	glPopMatrix();

	glColor3f(0.9f,0.9f,0.9f);
	glPushMatrix();
		glTranslatef(-9.5f,12.5f,0.9f);
		glRotatef(180,0,0,1);
		glScalef(0.5f,1.0f,0.05f);
		glutSolidSphere(3,100,100);
	glPopMatrix();

	glColor3f(0.9f,0.9f,0.9f);
	glPushMatrix();
		glTranslatef(-7.5f,12.3f,0.9f);
		glRotatef(200,0,0,1);
		glScalef(0.5f,1.0f,0.05f);
		glutSolidSphere(3,100,100);
	glPopMatrix();

	glColor3f(0.9f,0.9f,0.9f);
	glPushMatrix();
		glTranslatef(-6.0f,12.5f,0.9f);
		glRotatef(230,0,0,1);
		glScalef(0.5f,1.0f,0.05f);
		glutSolidSphere(3,100,100);
	glPopMatrix();

	glColor3f(0.9f,0.9f,0.9f);
	glPushMatrix();
		glTranslatef(-4.8f,13.5f,0.9f);
		glRotatef(260,0,0,1);
		glScalef(0.5f,1.0f,0.05f);
		glutSolidSphere(3,100,100);
	glPopMatrix();

	glColor3f(0.9f,0.9f,0.9f);
	glPushMatrix();
		glTranslatef(-4.0f,16.0f,0.9f);
		glScalef(1.0f,0.5f,0.05f);
		glutSolidSphere(3,100,100);
	glPopMatrix();

	glColor3f(0.9f,0.9f,0.9f);
	glPushMatrix();
		glTranslatef(-4.5f,17.5f,0.9f);
		glRotatef(30,0,0,1);
		glScalef(1.0f,0.5f,0.05f);
		glutSolidSphere(3,100,100);
	glPopMatrix();

	glColor3f(0.9f,0.9f,0.9f);
	glPushMatrix();
		glTranslatef(-4.5f,18.5f,0.9f);
		glRotatef(55,0,0,1);
		glScalef(1.0f,0.5f,0.05f);
		glutSolidSphere(3,100,100);
	glPopMatrix();

	glColor3f(0.9f,0.9f,0.9f);
	glPushMatrix();
		glTranslatef(-6.0f,19.5f,0.9f);
		glRotatef(75,0,0,1);
		glScalef(1.0f,0.5f,0.05f);
		glutSolidSphere(3,100,100);
	glPopMatrix();
}

void draw_bee() {
	
}

void display() {

	// Init and Enable stuff
	init_and_enable();

	// Drawing
	draw_grass();
	draw_flower1();
	glPushMatrix();
		glTranslatef(16.0f,0.4f,0.9f);
		draw_flower1();
	glPopMatrix();
	glutSwapBuffers();
	glFlush();
}

void keyboard(unsigned char k, int x, int y) {
	switch(k) {
		case 'd': y_rot = true;
				  y_angle = y_angle + 0.04;
				  break;
		case 'a': y_angle = y_angle - 0.04;
				  break;
		default:
				  break;
	}

	glutPostRedisplay();
}

void reshape(int w, int h) {
	GLfloat fAspect;
	if(h == 0)
		h = 1;
	glViewport(0,0,w,h);
	fAspect = (GLfloat)w/(GLfloat)h;
	glMatrixMode(GL_PROJECTION);
	gluPerspective(60.0f,fAspect,1.0f,400.0f);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(0.0f,-10.0f,-50.0f);
}


int main(int argc,char** argv) {
	glutInit(&argc,argv);
	glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE | GLUT_DEPTH);
	glutInitWindowSize(1024,600);
	glutInitWindowPosition(184,100);
	glutCreateWindow("Flower");
	glutDisplayFunc(display);
	glutKeyboardFunc(keyboard);
	glutReshapeFunc(reshape);
	glutMainLoop();
}