#include <GL/glut.h>
#include <GL/glu.h>
#include <GL/gl.h>

#include <math.h>

#define GEAR 1
#define PI 3.1415

#define GEAR_1 2
#define GEAR_2 3
#define BUFFER_LENGTH 64

GLfloat height = 5.0f;
GLfloat radius = 10.0f;
GLfloat iAngle = 0;

GLfloat y_angle = 0;
GLfloat z_angle = 1;
GLfloat x_rot = 0;

GLfloat ambient[4] = {0.5f,0.5f,0.5f,1.0f};
GLfloat dSpecular[4] = {1.0,1.0,1.0,1.0};
GLfloat dAmbient[4] = {0.3,0.3,0.3,1.0};
GLfloat dDiffuse[4] = {0.4,0.4,0.4,1.0};
GLfloat light_pos[4] = {50.0,50.0,-50,1.0f};

GLfloat inc = 1;

bool gRotation = true;
bool bLight = true;
bool sceneRot = false;


void draw_gear() {
	glShadeModel(GL_SMOOTH);

	glInitNames();
	glPushName(0);

	glNewList(GEAR,GL_COMPILE);
		glShadeModel(GL_SMOOTH);
		glRotatef(y_angle,0,1,0);

		GLUquadricObj *quadric;
		quadric = gluNewQuadric();
		gluCylinder(quadric,radius,radius,height,50,50);

		glBegin(GL_TRIANGLE_FAN);
				for(int i = 0; i <= 360; i++) {
				GLfloat angle = 2*PI*i/360;
				GLfloat x = cos(angle)*10;
				GLfloat y = sin(angle)*10;
				glVertex3f(x,y,0);
			}
		glEnd();

		glPushMatrix();
			glTranslatef(0,0,5);
			glBegin(GL_TRIANGLE_FAN);
					for(int i = 0; i <= 360; i++) {
					GLfloat angle = 2*PI*i/360;
					GLfloat x = cos(angle)*10;
					GLfloat y = sin(angle)*10;
					glVertex3f(x,y,0);
				}
			glEnd();
		glPopMatrix();

		glPushMatrix();
		for(int i = 0; i < 4; i++) {
			GLfloat angle = 2*PI*i/4;
			GLfloat x = cos(angle)*10;
			GLfloat y = sin(angle)*10;
			glPushMatrix();
				glTranslatef(x,y,2.5);
				glutSolidCube(4);
			glPopMatrix();
		}

		glRotatef(45,0,0,1);
		for(int i = 0; i < 4; i++) {
			GLfloat angle = 2*PI*i/4;
			GLfloat x = cos(angle)*10;
			GLfloat y = sin(angle)*10;
			glPushMatrix();
				glTranslatef(x,y,2.5);
				glutSolidCube(4);
			glPopMatrix();
		}
		glPopMatrix();

	glEndList();

	glRotatef(x_rot,sceneRot,0,0);

	// gear 1
	glPushMatrix();
		glColor3f(1,0,0);
		glRotatef(-z_angle,0,0,1);
		glCallList(GEAR);
		glLoadName(GEAR_1);
	glPopMatrix();

	// gear 2
	glPushMatrix();
		glColor3f(0,1,0);
		glTranslatef(22.5,4.6,0);
		glRotatef(z_angle,0,0,1);
		glCallList(GEAR);
		glLoadName(GEAR_2);
	glPopMatrix();
}

void render() {
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glClearColor(0.0f,0.0f,0.0f,1.0f);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);
	glEnable(GL_LIGHTING);
	
	if(bLight) glEnable(GL_LIGHT0);
	if(!bLight) glDisable(GL_LIGHT0);
	
	glLightModelfv(GL_LIGHT_MODEL_AMBIENT,ambient);
	glLightfv(GL_LIGHT0,GL_SPECULAR,dSpecular);
	glLightfv(GL_LIGHT0,GL_AMBIENT,dAmbient);
	glLightfv(GL_LIGHT0,GL_DIFFUSE,dDiffuse);
	glLightfv(GL_LIGHT0,GL_POSITION,light_pos);
	
	glColorMaterial(GL_FRONT_AND_BACK,GL_AMBIENT_AND_DIFFUSE);
	glMateriali(GL_FRONT,GL_SHININESS,100);

	draw_gear();

	glFlush();
	glutSwapBuffers();
}

void keyboard(unsigned char k, int x,int y) {
	switch(k) {
		case '+': 
				 inc = inc + 0.5;
				 break;
		case '-':
				 inc = inc - 0.5;
				 break;
		case 'l':
				 bLight = !bLight;
		case 'q':
				 sceneRot = true;
				 x_rot = x_rot + 0.5;
				 break;
		case 'e': 
				 sceneRot = true;
				 x_rot = x_rot - 0.5;
				 break;
		default:
				 break;

	}
}

void reshape(int w,int h){
	GLfloat fAspect;
	if(h == 0)
		h = 1;
	glViewport(0,0,w,h);
	fAspect = (GLfloat)w/(GLfloat)h;
	glMatrixMode(GL_PROJECTION);
	gluPerspective(60.0f,fAspect,1.0f,400.0f);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(0.0f,-5.0f,-50.0f);
}

void animation() {
	z_angle = z_angle + inc;

	glutPostRedisplay();
}

void process_picking(GLuint *buffer) {
	GLint id, counter;

	counter = buffer[0];
	id = buffer[2];

	switch(id) {
		case GEAR_1:
				z_angle = -z_angle;
				break;
		case GEAR_2:
				z_angle = -z_angle;
		default:
				break;
	}
}

void process_selection()

int main(int argc,char** argv) {
	glutInit(&argc,argv);
	glutInitDisplayMode(GLUT_RGBA | GLUT_DEPTH | GLUT_DOUBLE);
	glutInitWindowSize(1024,600);
	glutInitWindowPosition(184,100);
	glutCreateWindow("Ingranaggi");
	glutDisplayFunc(render);
	glutIdleFunc(animation);
	glutKeyboardFunc(keyboard);
	glutReshapeFunc(reshape);
	glutMainLoop();
}