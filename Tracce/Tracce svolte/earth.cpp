#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glut.h>
#include <stdlib.h>

int random(int m) {
	return rand()%m;
}

void init_and_enable() {
	glEnable(GL_DEPTH_TEST);
}

void draw_space() {
	for(int i=0; i<100; i++) {
		glPointSize(0.5);
		glBegin(GL_POINTS);
			glColor3f(1.0f,1.0f,1.0f);
			glVertex2i(random(100),random(100));
		glEnd();
	}
}

void draw_earth() {
	glShadeModel(GL_SMOOTH);
	glPushMatrix();
		glColor3f(0.0f,0.0f,0.2f);
		glTranslatef(0.0f,7.0f,0.0f);
		glutSolidSphere(10,200,200);
	glPopMatrix();
}

void display() {
	glClearColor(0.0f,0.0f,0.02f,1.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	draw_space();
	draw_earth();
	

	glFlush();
	glutSwapBuffers();
}

void reshape(int w, int h) {
	GLfloat fAspect;
	if(h == 0)
		h = 1;
	glViewport(0,0,w,h);
	fAspect = (GLfloat) w/(GLfloat) h;
	glMatrixMode(GL_PROJECTION);
	gluPerspective(60.0f,fAspect,1.0f,400.0f);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(0.0f,-10.0f,-50.0f);
}

int main(int argc,char** argv) {
	glutInit(&argc,argv);
	glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE | GLUT_DEPTH);
	glutInitWindowSize(1024,600);
	glutInitWindowPosition(184,100);
	glutCreateWindow("Earth in space");
	glutReshapeFunc(reshape);
	glutDisplayFunc(display);
	glutMainLoop();
}