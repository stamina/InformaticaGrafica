#include <GL/glut.h>
#include <GL/glu.h>
#include <GL/gl.h>

#define BOAT 1

bool sceneRot = false;
bool y_oscilmax = false;
bool anim = false;

GLfloat x_rot = 0;
GLfloat x_t = 0;

GLfloat y_oscil = 0;

GLfloat theta_down = 0;
GLfloat y_barca = 0;

GLfloat ambient[4] = {0.8f,0.8f,0.8f,1.0f};
GLfloat dSpecular[4] = {1.0,1.0,1.0,1.0};
GLfloat dAmbient[4] = {0.3,0.3,0.3,1.0};
GLfloat dDiffuse[4] = {0.4,0.4,0.4,1.0};
GLfloat light_pos[4] = {16.0f,20.0f,0.0f, 1.0f};


void draw_boat() {

	glPushMatrix();
		glRotatef(x_rot,0,sceneRot,0);
	glPopMatrix();

	glNewList(BOAT,GL_COMPILE);
	// Barca
	glPushMatrix();
		glColor3f(0.3f,0.09f,0.0f);
		glTranslatef(-20.0f,-1.6,0.0f);
		glScalef(5.0f,-1.0f,3.0f);
		glutSolidCube(2);
	glPopMatrix();

	// Albero
	glPushMatrix();
		glColor3f(0.5f,0.16f,0.0f);
		glScalef(1,20,0.5);
		glTranslatef(-20.0f,0.2,0);
		glutSolidCube(0.5);
	glPopMatrix();

	// Albero 2
	glPushMatrix();
		glColor3f(0.5f,0.16f,0.0f);
		glScalef(1,0.5,10);
		glTranslatef(-20.0f,4,0);
		glutSolidCube(0.5);
	glPopMatrix();

	// Vela
	
	glColor3f(0.6f,0.6f,0.6f);
	glBegin(GL_TRIANGLES);
		glVertex3f(-20.0f,1.8,-3);
		glVertex3f(-20.0f,10,0);
		glVertex3f(-17.0f,1.8,-0.001);
	glEnd();

	glBegin(GL_TRIANGLES);
		glVertex3f(-20.0f,1.8,3);
		glVertex3f(-20.0f,10,0);
		glVertex3f(-17.0f,1.8,0.001);
	glEnd();

	glEndList();

	glPushMatrix();
		glTranslatef(x_t,y_oscil,0);
		glTranslatef(0,y_barca,0);
		glRotatef(-theta_down,0,0,1);
		glCallList(BOAT);
	glPopMatrix();
}

void draw_sun() {
	glPushMatrix();
		glColor3f(1.0f,0.8f,0.0f);
		glTranslatef(16,20,0);
		glutSolidSphere(5,200,200);
	glPopMatrix();
}

void draw_sea() {
	glPushMatrix();	
		glColor3f(0.0f,0.0f,1.0f);
		glScalef(7,-1,3);
		glTranslatef(0,10,0);
		glutSolidCube(15);
	glPopMatrix();
	/*
	glNewList(SEA,GL_COMPILE);
		glBegin(GL_TRIANGLES);
			glVertex2f(-1.0f,0.0f);
			glVertex2f(0.0f,1.0f);
			glVertex2f(1.0f,0.0f);
		glEnd();
	glEndList();

	glCallList(SEA);
	*/
}

void renderScene() {
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glClearColor(0.0f,0.8f,0.8f,1.0f);

	glEnable(GL_DEPTH_TEST);
	glEnable(GL_LIGHTING);
	glEnable(GL_COLOR_MATERIAL);
	
		glLightModelfv(GL_LIGHT_MODEL_AMBIENT,ambient);
		glLightfv(GL_LIGHT0,GL_SPECULAR,dSpecular);
		glLightfv(GL_LIGHT0,GL_AMBIENT,dAmbient);
		glLightfv(GL_LIGHT0,GL_DIFFUSE,dDiffuse);
		glLightfv(GL_LIGHT0,GL_POSITION,light_pos);
		
		glRotatef(x_rot,0,sceneRot,0);

		draw_sun();
		draw_sea();
		draw_boat();

		glColorMaterial(GL_FRONT_AND_BACK,GL_AMBIENT_AND_DIFFUSE);	

	glFlush();
	glutSwapBuffers();
}

void reshape(int w,int h){
	GLfloat fAspect;
	if(h == 0)
		h = 1;
	glViewport(0,0,w,h);
	fAspect = (GLfloat)w/(GLfloat)h;
	glMatrixMode(GL_PROJECTION);
	gluPerspective(60.0f,fAspect,1.0f,400.0f);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(0.0f,-5.0f,-50.0f);
}

void keyboard(unsigned char k, int x, int y) {
	switch(k) {
		case 'q':
				 sceneRot = true;
				 x_rot = x_rot + 1;
				 break;
		case 'e': 
				 sceneRot = true;
				 x_rot = x_rot - 1;
				 break;
		case 'g':
				 anim = true;
				 break;
		case 'f':
				 anim = false;
				 break;
		default:
				 break;
	}

	glutPostRedisplay();
}

void animation() {
	if(anim) {
		if(x_t < 23.0f)
			x_t = x_t + 0.08;


		if(y_oscilmax)
			y_oscil = y_oscil + 0.01;
		else
			y_oscil = y_oscil - 0.01;

		if(y_oscil >= 0.001) y_oscilmax = false;
		if(y_oscil <= -0.6) y_oscilmax = true;

		if(x_t > 23.0f && theta_down < 90) 
			theta_down = theta_down + 0.2;

		if(x_t >= 23.0f)
			y_barca= y_barca - 0.1;
		glutPostRedisplay();
	}
}

int main(int argc,char** argv) {
	glutInit(&argc,argv);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA | GLUT_DEPTH);
	glutInitWindowSize(1024,700);
	glutInitWindowPosition(184,100);
	glutCreateWindow("Barca sul mare");
	glutReshapeFunc(reshape);
	glutKeyboardFunc(keyboard);
	glutIdleFunc(animation);
	glutDisplayFunc(renderScene);
	glutMainLoop();
}