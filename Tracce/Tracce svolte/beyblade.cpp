#include <GL/glut.h>
#include <GL/glu.h>
#include <GL/gl.h>

#define TABLE 1
#define ROOF 2
#define FOOT 3
#define BEY 4

GLfloat y_rot = 0;
GLfloat bey_rot = 0;
GLfloat bey_x = 0;
GLfloat bey_z = 0;
GLfloat tav_x = 0;
GLfloat tav_z = 0;

GLfloat ambient[4] = {0.3f,0.3f,0.3f,1.0f};
GLfloat directional[4] = {1.0f,0.2,0.0,0.0};
GLfloat light_pos[4] = {0.0,10.0,-5.0,1.0f};


void draw_roof() {

	glRotatef(y_rot,0,1,0);

	glNewList(ROOF,GL_COMPILE);
		glColor3f(0.5f,0.5f,0.5f);
		glPushMatrix();
			glScalef(15,0.5,15);
			glTranslatef(0,-10.0f,0);
			glutSolidCube(2);
		glPopMatrix();
	glEndList();

	glPushMatrix();
		glCallList(ROOF);
	glPopMatrix();
}

void draw_table() {
	glPushMatrix();
		glRotatef(y_rot,0,1,0);
		glTranslatef(tav_x,0,tav_z);
			glColor3f(0.47f,0.28f,0.09);
			glPushMatrix();
				glScalef(10,0.8,10);
				glTranslatef(0,5,0);
				glutSolidCube(1);
			glPopMatrix();
	glPopMatrix();
}

void draw_foots() {
	glNewList(FOOT,GL_COMPILE);
		glColor3f(0.47f,0.28f,0.09);
		glTranslatef(tav_x,0,tav_z);
		glPushMatrix();
			glScalef(1,8.5,1);
			glutSolidCube(1);
		glPopMatrix();
	glEndList();

	glPushMatrix();
		glTranslatef(4.3,0,4.3);
		glCallList(FOOT);
	glPopMatrix();

	glPushMatrix();
		glTranslatef(-4.3,0,4.3);
		glCallList(FOOT);
	glPopMatrix();

	glPushMatrix();
		glTranslatef(4.3,0,-4.3);
		glCallList(FOOT);
	glPopMatrix();

	glPushMatrix();
		glTranslatef(-4.3,0,-4.3);
		glCallList(FOOT);
	glPopMatrix();
}


void draw_bey() {
	glPushMatrix();
		glTranslatef(tav_x,0,tav_z);
		glColor3f(0,0,1);
		glPushMatrix();
			glTranslatef(0,6.5,0);
				glTranslatef(bey_x,0,bey_z);
				glRotatef(bey_rot,0,1,0);
				glPushMatrix();
					glRotatef(90,1,0,0);
				glutSolidCone(1,2,30,30);		
			glPopMatrix();
		glPopMatrix();

		glColor3f(0,1,0);
		glPushMatrix();
			glTranslatef(0,6.5,0);
			glTranslatef(bey_x,0,bey_z);
			glRotatef(bey_rot,0,1,0);
			glPushMatrix();
				glutSolidCube(1);
			glPopMatrix();
		glPopMatrix();
	glPopMatrix();
}


void renderScene() {
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glClearColor(0.8f,0.8f,0.8f,1.0f);

	glEnable(GL_DEPTH_TEST);
	glEnable(GL_LIGHTING);
	glEnable(GL_NORMALIZE);
	glEnable(GL_LIGHT0);
	glEnable(GL_COLOR_MATERIAL);

	glLightModelfv(GL_LIGHT_MODEL_AMBIENT,ambient);
	//glLightfv(GL_LIGHT0,GL_AMBIENT,directional);
	//glLightfv(GL_LIGHT0,GL_DIFFUSE,directional);
	glLightfv(GL_LIGHT0,GL_SPECULAR,directional); 
	glLightfv(GL_LIGHT0,GL_POSITION,light_pos);
	
	glColorMaterial(GL_FRONT_AND_BACK,GL_AMBIENT_AND_DIFFUSE);	

	draw_roof();
	draw_foots();
	draw_bey();
	draw_table();
	

	glFlush();
	glutSwapBuffers();
}

void keyboard(unsigned char k, int x, int y) {
	switch(k) {
		case 'z':
				y_rot = y_rot + 2;
				break;
		case 'x':
				y_rot = y_rot - 2;
				break;
		case 'd':
				if(bey_x < 4.51)bey_x = bey_x + 0.5;
				break;
		case 'a':
				if(bey_x > - 4.51)bey_x = bey_x - 0.5;
				break;
		case 'w':
				if(bey_z < 4.51)bey_z = bey_z + 0.5;
				break;
		case 's':
				if(bey_z > -4.51)bey_z = bey_z - 0.5;
		default:
		break;
	}

	glutPostRedisplay();
}

void special_keys(int k, int x, int y) {
	switch(k) {
		case GLUT_KEY_RIGHT: 
			if(tav_x < 10) tav_x = tav_x + 0.5;
			break;
		case GLUT_KEY_LEFT:
			if(tav_x > -10) tav_x = tav_x - 0.5;
			break;
		case GLUT_KEY_UP:
			if(tav_z < 10) tav_z = tav_z + 0.5;
			break;
		case GLUT_KEY_DOWN:
			if(tav_z > -10) tav_z = tav_z - 0.5;
		default:
		break;
	}
}

void reshape(int w,int h){
	GLfloat fAspect;
	if(h == 0)
		h = 1;
	glViewport(0,0,w,h);
	fAspect = (GLfloat)w/(GLfloat)h;
	glMatrixMode(GL_PROJECTION);
	gluPerspective(50.0f,fAspect,6.0f,800.0f);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(0.0f,-10.0f,-50.0f);
}

void idle() {
	bey_rot = bey_rot + 1;

	glutPostRedisplay();
}

int main(int argc,char** argv) {
	glutInit(&argc,argv);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA | GLUT_DEPTH);
	glutInitWindowSize(1024,700);
	glutInitWindowPosition(190,100);
	glutCreateWindow("Beyblade");
	glutKeyboardFunc(keyboard);
	glutIdleFunc(idle);
	glutSpecialFunc(special_keys);
	glutReshapeFunc(reshape);
	glutDisplayFunc(renderScene);
	glutMainLoop();
}