#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glut.h>
#include <iostream>

GLfloat angle_x = 0;
GLfloat angle_y = 0;

GLfloat light_pos[4] = {-20.0f, 20.0f, 0.0f, 1.0f};
GLfloat low_light[4] = {0.5,0.5,0.5,0.5};
GLfloat white_light[4] = {1,1,1,1};

GLfloat x_move = 0;
GLfloat z_move = 0;

bool rot_x = false;
bool rot_y = false;

void draw_chess_plate() {
	

	// Limite
	GLfloat max = 20.0f;
	// Passo
    GLfloat step = 5.0f;
    // Spessore
    GLfloat y = 0.0f;
    // Colore
    GLfloat color = 0;
    // Assi su cui disegnare
    GLfloat x, z = 0;
    // Contatore per colore
    GLint counter = 0;

    glRotatef(angle_x,rot_x,0,0);
    glRotatef(angle_y,0,rot_y,0);
    rot_x = false;
    rot_y = false;

    	glShadeModel(GL_FLAT);
		for(x = -max; x < max; x += step) {
			glBegin(GL_TRIANGLE_STRIP);
			for(z = max; z >= -max; z -= step) {
				if((counter %2) == 0)
					color = 0.0f;
				else
					color = 1.0f;
				glColor3f(color,color,color);
					glVertex3f(x,y,z);
					glVertex3f(x + step,y,z);
					counter++;
			}
			glEnd();
		}
		glShadeModel(GL_SMOOTH);
		// Pedine
		for(x = -max; x < max; x += step) {
			for(z = max; z >= -max/2; z -= step) {
				glPushMatrix();
					glColor3f(0.6,0.6,0.6);
					glTranslatef(x+2.5,1,max-7.5);
					glutSolidCube(1.2);
				glPopMatrix();
			}
		}

		// BIANCHI
		
		glColor3f(0.6,0.6,0.6);

		// Torri
		glPushMatrix();
			glScalef(1,1,1);
			glTranslatef(-17.5,1,17.5);
			glTranslatef(x_move,0,z_move);
			glutSolidTetrahedron();
		glPopMatrix();

		glPushMatrix();
		glScalef(1,1,1);
			glTranslatef(17.5,1,17.5);
			glutSolidTetrahedron();
		glPopMatrix();

		// Cavalli
		glPushMatrix();
			glTranslatef(-12.5,1,17.5);
			glutSolidSphere(1,10,10);
		glPopMatrix();

		glPushMatrix();
			glTranslatef(12.5,1,17.5);
			glutSolidSphere(1,10,10);
		glPopMatrix();

		// Alfieri
		glPushMatrix();
			glTranslatef(-7.5,0,17.5);
			glRotatef(-90,1,0,0);
			glutSolidCone(1,2,10,10);
		glPopMatrix();

		glPushMatrix();
			glTranslatef(7.5,0,17.5);
			glRotatef(-90,1,0,0);
			glutSolidCone(1,2,10,10);
		glPopMatrix();

		// Re
		glPushMatrix();
			glTranslatef(-2.5,1,17.5);
			glutSolidTorus(0.3,0.7,60,40);
		glPopMatrix();

		// Regina
		glPushMatrix();
			glTranslatef(2.5,1,17.5);
			glRotatef(90,0,1,0);
			glutSolidTeapot(1);
		glPopMatrix();


		// NERI

		// Pedine
		for(x = -max; x < max; x += step) {
			for(z = max; z >= -max/2; z -= step) {
				glPushMatrix();
					glColor3f(0.2,0.05,0);
					glTranslatef(x+2.5,1,max-32.5);
					glutSolidCube(1.2);
				glPopMatrix();
			}
		}

		glColor3f(0.2,0.05,0);
		
		// Torri
		glPushMatrix();
			glScalef(1,1,1);
			glTranslatef(-17.5,1,-17.5);
			glutSolidTetrahedron();
		glPopMatrix();

		glPushMatrix();
		glScalef(1,1,1);
			glTranslatef(17.5,1,-17.5);
			glutSolidTetrahedron();
		glPopMatrix();

		// Cavalli
		glPushMatrix();
			glTranslatef(-12.5,1,-17.5);
			glutSolidSphere(1,10,10);
		glPopMatrix();

		glPushMatrix();
			glTranslatef(12.5,1,-17.5);
			glutSolidSphere(1,10,10);
		glPopMatrix();

		// Alfieri
		glPushMatrix();
			glTranslatef(-7.5,0,-17.5);
			glRotatef(-90,1,0,0);
			glutSolidCone(1,2,10,10);
		glPopMatrix();

		glPushMatrix();
			glTranslatef(7.5,0,-17.5);
			glRotatef(-90,1,0,0);
			glutSolidCone(1,2,10,10);
		glPopMatrix();

		// Re
		glPushMatrix();
			glTranslatef(-2.5,1,-17.5);
			glutSolidTorus(0.3,0.7,60,40);
		glPopMatrix();

		// Regina
		glPushMatrix();
			glTranslatef(2.5,1,-17.5);
			glRotatef(-90,0,1,0);
			glutSolidTeapot(1);
		glPopMatrix();

	glutSwapBuffers();
	glFlush();
}

void reshape(int w, int h) {
    GLfloat fAspect;
    if(h == 0)
        h = 1;
    glViewport(0, 0, w, h);   
    fAspect = (GLfloat)w / (GLfloat)h;
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(60.0f, fAspect, 1.0f, 400.0f);   
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();    
    glTranslatef(0.0f, -10.0f, -50.0f);
}


void keyboard(unsigned char key,int x,int y) {
	switch(key) {	
		case 'w': angle_x = 5;
				  rot_x = true;
				  rot_y = false;
			break;
		case 's': angle_x = 5;
				  rot_x = true;
				  rot_y = false;
			break;
		case 'd': angle_y = 5;
				  rot_y = true;
				  rot_x = false;
			break;
		case 'a': angle_y = 5;
				  rot_y = true;
				  rot_x = false;
			break;
		case 
		default:
			break;
	}

	glutPostRedisplay();
}

void display() {
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glClearColor(0.4,0.4,0.4,1.0);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL); 
	glColorMaterial(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE);
	glEnable(GL_LIGHTING);

	
	glLightModelfv(GL_LIGHT_MODEL_AMBIENT, low_light);
	glLightfv(GL_LIGHT0,GL_DIFFUSE,white_light);
	glLightfv(GL_LIGHT0,GL_SPECULAR,white_light);
	glLightfv(GL_LIGHT0,GL_POSITION,light_pos);
	glEnable(GL_LIGHT0);

	glEnable(GL_COLOR_MATERIAL);
	glColorMaterial(GL_FRONT, GL_AMBIENT_AND_DIFFUSE);
    glMaterial(GL_FRONT, GL_SHININESS, 128);

	draw_chess_plate();
}

int main(int argc,char** argv) {
	glutInit(&argc,argv);
	glutInitDisplayMode(GLUT_RGB | GLUT_DEPTH | GLUT_DOUBLE);
	glutInitWindowSize(1024,600);
	glutInitWindowPosition(100,100);
	glutCreateWindow("Scacchiera");
	glutReshapeFunc(reshape);
	glutKeyboardFunc(keyboard);
	glutDisplayFunc(display);
	glutMainLoop();
}