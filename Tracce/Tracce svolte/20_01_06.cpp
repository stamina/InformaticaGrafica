#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glut.h>
#include <iostream>

GLfloat var_base = 0;
GLfloat var_height = 0;

GLfloat radius = 0;
GLfloat x_rot = 0;
GLfloat y_rot = 0;
GLfloat z_rot = 0;
GLfloat angle = 0;

GLuint factor = 1;
GLushort pattern = 0xA30F;

bool wireframe = false;
bool stipple = false;

void draw_prism() {
	glClearColor(0.0,0.0,0.0,1.0);
	glClear(GL_COLOR_BUFFER_BIT);	

	// Creazione del prisma
	if(wireframe) {
		glPolygonMode(GL_FRONT_AND_BACK,GL_LINE);
		if(stipple) {
			glEnable(GL_LINE_STIPPLE);
			glLineStipple(factor,pattern);
		}
		else
			glDisable(GL_LINE_STIPPLE);
	}
	else 
		glPolygonMode(GL_FRONT_AND_BACK,GL_FILL);

		glBegin(GL_POLYGON);
			glVertex3f(0.3,0.0,-0.3);
			glVertex3f(0.3,0.3,0.0);
			glVertex3f(-0.3,0.0,0.0);
		glEnd();

	 glFlush();
	 glutSwapBuffers();
}

void display() {
	draw_prism();
}

void keyboard_f(unsigned char k, int x, int y) {
	switch(k) {
		std::cout <<"diocane" << std::endl;
		case 32: wireframe = !wireframe;
			break;
		case 'u': stipple = !stipple;
			break;
		default:
			break;
	}

	glutPostRedisplay();
}

int main(int argc,char** argv) {
	glutInit(&argc,argv);
	glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE);
	glutInitWindowSize(600,600);
	glutInitWindowPosition(100,100);
	glutCreateWindow("Prisma con circonferenza");
	glutKeyboardFunc(keyboard_f);
	glutDisplayFunc(display);
	glutMainLoop();
}