#include <GL/glut.h>
#include <GL/glu.h>
#include <GL/gl.h>
#include <iostream>
#include <cmath>
#include <climits>

void process_zObject(GLuint *selectBuffer, GLint hits) {
	int id = 0;
	GLuint z_min = UINT_MAX; // Sparo al massimo la z
	GLuint id_min = INT_MIN; // Sparo al minimo l'id

	for(int i=0; i<hits; i++) {
		for(int j=id; j<=(id+2+selectBuffer[id]); j++) {
			std::cout << selectBuffer[j] << std::endl;
		}

		std::cout << std::endl;
		// Se il valore di z nel buffer
		if(selectBuffer[id+1] < z_min) {
			// Prendere il valore della z_min nel buffer
			z_min = selectBuffer[id+1];
			id_min = selectBuffer[id+2+selectBuffer[id]];
		}

		id += selectBuffer[id]+3;
	}

	std::cout << "Selected object: " << id_min << std::endl;

	glutPostRedisplay();
}