#include <GL/glut.h>
#include <GL/glu.h>
#include <GL/gl.h>


GLfloat zrot = 0;
GLfloat yrot = 0;
GLfloat rotSX = 0;
GLfloat rotDX = 0;


void draw_cubes(){
	// polso
	glPushMatrix();
	glColor3ub(255,255,255);
	glScalef(3,1,2);
	glutWireCube(4.0f);
	glPopMatrix();

	// dito sx
	glPushMatrix();
	glColor3ub(255,255,255);
	glTranslatef(-3,0,4);
	glRotatef(rotSX,1,0,0);
	glTranslatef(0,0,4);
	glScalef(0.9,0.7,2);
	glutWireCube(4.0f);
	glPopMatrix();

	// dito dx
	glPushMatrix();
	glColor3ub(255,255,255);
	glTranslatef(+3,0,4);
	glRotatef(rotDX,1,0,0);
	glTranslatef(0,0,4);
	glScalef(0.9,0.7,2);
	glutWireCube(4.0f);
	glPopMatrix();

	glColor3f(1,0,0);
	glBegin(GL_LINES);
		glVertex3f(0,0,0);
		glVertex3f(2,0,0);
	glEnd();

	glColor3f(0,1,0);
	glBegin(GL_LINES);
		glVertex3f(0,0,0);
		glVertex3f(0,2,0);
	glEnd();

	glColor3f(0,0,1);
	glBegin(GL_LINES);
		glVertex3f(0,0,0);
		glVertex3f(0,0,2);
	glEnd();


}


void draw_lines(){
	
	//assi
	glColor3f(1,0,0);
	glBegin(GL_LINES);
		glVertex3f(0,0,0);
		glVertex3f(1,0,0);
	glEnd();

	glColor3f(0,1,0);
	glBegin(GL_LINES);
		glVertex3f(0,0,0);
		glVertex3f(0,1,0);
	glEnd();

	glColor3f(0,0,1);
	glBegin(GL_LINES);
		glVertex3f(0,0,0);
		glVertex3f(0,0,1);
	glEnd();

	// polso
	glColor3f(1,1,1);
	glBegin(GL_LINE_STRIP);	
		glVertex3f(-2.0f,-0.5f,1.0); // v3
		glVertex3f(+2.0f,-0.5f,1.0); // v6
		glVertex3f(+2.0f,+0.5f,1.0); // v7
		glVertex3f(-2.0f,0.5f,1.0); // v4
		glVertex3f(-2.0f,-0.5f,1.0); // v3
		glVertex3f(-2.0f,-0.5f,-1.0); // v2
		glVertex3f(-2.0f,+0.5f,-1.0); // v1
		glVertex3f(-2.0f,0.5f,1.0); // v4
		glVertex3f(+2.0f,+0.5f,1.0); // v7
		glVertex3f(+2.0f,0.5f,-1.0); // v8
		glVertex3f(+2.0f,-0.5f,-1.0); // v5
		glVertex3f(+2.0f,-0.5f,1.0); // v6
		glVertex3f(+2.0f,-0.5f,-1.0); // v5
		glVertex3f(-2.0f,-0.5f,-1.0); // v2
		glVertex3f(-2.0f,+0.5f,-1.0); // v1
		glVertex3f(+2.0f,0.5f,-1.0); // v8
	glEnd();

	// dito sx
	glBegin(GL_LINE_STRIP);	
		glVertex3f(-1.5f,-0.25f,6.0); // v3
		glVertex3f(-0.5f,-0.25f,6.0); // v6
		glVertex3f(-0.5f,+0.25f,6.0); // v7
		glVertex3f(-1.5f,0.25f,6.0); // v4
		glVertex3f(-1.5f,-0.25f,6.0); // v3
		glVertex3f(-1.5f,-0.25f,1.0); // v2
		glVertex3f(-1.5f,+0.25f,1.0); // v1
		glVertex3f(-1.5f,0.25f,6.0); // v4
		glVertex3f(-0.5f,+0.25f,6.0); // v7
		glVertex3f(-0.5f,0.25f,1.0); // v8
		glVertex3f(-0.5f,-0.25f,1.0); // v5
		glVertex3f(-0.5f,-0.25f,6.0); // v6
		glVertex3f(-0.5f,-0.25f,1.0); // v5
		glVertex3f(-1.5f,-0.25f,1.0); // v2
		glVertex3f(-1.5f,+0.25f,1.0); // v1
		glVertex3f(-0.5f,0.25f,1.0); // v8
	glEnd();

	//dito dx
	glBegin(GL_LINE_STRIP);	
		glVertex3f(0.5f,-0.25f,6.0); // v3
		glVertex3f(1.5f,-0.25f,6.0); // v6
		glVertex3f(1.5f,+0.25f,6.0); // v7
		glVertex3f(0.5f,0.25f,6.0); // v4
		glVertex3f(0.5f,-0.25f,6.0); // v3
		glVertex3f(0.5f,-0.25f,1.0); // v2
		glVertex3f(0.5f,+0.25f,1.0); // v1
		glVertex3f(0.5f,0.25f,6.0); // v4
		glVertex3f(1.5f,+0.25f,6.0); // v7
		glVertex3f(1.5f,0.25f,1.0); // v8
		glVertex3f(1.5f,-0.25f,1.0); // v5
		glVertex3f(1.5f,-0.25f,6.0); // v6
		glVertex3f(1.5f,-0.25f,1.0); // v5
		glVertex3f(0.5f,-0.25f,1.0); // v2
		glVertex3f(0.5f,+0.25f,1.0); // v1
		glVertex3f(1.5f,0.25f,1.0); // v8
	glEnd();
}



void display(){
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);

	glLoadIdentity();

	glTranslatef(-2,0,0);

	glRotatef(zrot,0,0,1);
	glRotatef(yrot,0,1,0);

	glTranslatef(2,0,0);

	draw_cubes();
	glutSwapBuffers(); // al posto di flush() se usi double
}

void reshape(int w, int h) {
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glViewport(0,0,w,h);
	gluPerspective(45.0f,(float)w/h,1.0f,100.0f); 
	glTranslatef(0,0,-30.0f); // valore negativo per allontanarmi (solo nella reshape)
}

void keyboard(unsigned char k, int x, int y){
	switch(k){
		case 'z':
			zrot = zrot + 0.45;
			break;
		case 'y':
			yrot = yrot + 0.45;
			break;	
		case 'a':
			rotSX = rotSX + 0.45;
			if (rotSX > 60) rotSX = (60);
			break;
		case 'q':
			rotSX = rotSX - 0.45;
			if (rotSX < -30) rotSX = (-30);	
			break;
		case 's':
			rotDX = rotDX + 0.45;
			if (rotDX > 60) rotDX = (60);
			break;
		case 'w':
			rotDX = rotDX - 0.45;
			if (rotDX < -30) rotDX = (-30);	
			break;			
	}
	glutPostRedisplay();
}

bool init()
{
	glClearColor(0.10f, 0.10f, 0.10f, 0.0f);
       // glEnable(GL_CULL_FACE); // per le superifici nascoste
        glEnable(GL_DEPTH_TEST);   
	return true;
}

int main(int argc,char** argv) {
	glutInit(&argc,argv);
	glutInitDisplayMode(GLUT_RGB | GLUT_DEPTH | GLUT_DOUBLE);
	glutInitWindowSize(600,600);
	glutInitWindowPosition(100,200);
	glutCreateWindow("Mano");
	// glutMouseFunc(mouse_button);
	// glutMotionFunc(mouse_move);
	glutKeyboardFunc(keyboard);
	glutDisplayFunc(display);
	glutReshapeFunc(reshape);

	if(!init())
		return 1;

	glutMainLoop();
}