#include <GL/glut.h>
#include <GL/glu.h>
#include <GL/gl.h>
#include <math.h>

void display() {
	glClearColor(0,0,0,1);
	glClear(GL_COLOR_BUFFER_BIT);

	GLfloat l = 2*0.9/sqrt(3);

	glColor3f(1,0,0);
	glutSolidCube(l);


	glColor3f(1,1,1);
	glutWireSphere(0.9,40,40);


	glFlush();
}

int main(int argc,char** argv) {
	glutInit(&argc,argv);
	glutInitDisplayMode(GLUT_RGB);
	glutInitWindowSize(500,500);
	glutInitWindowPosition(100,200);
	glutCreateWindow("Bandiera e cerchio");
	glutDisplayFunc(display);
	glutMainLoop();
}