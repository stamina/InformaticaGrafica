#include <GL/glut.h>
#include <GL/glu.h>
#include <GL/gl.h>

GLfloat x_angle = 0;
GLfloat y_angle = 0;

GLfloat xdiff = 0;
GLfloat ydiff = 0;

bool press = false;

void draw_box() {

	glColor3f(1,0,0);
	glBegin(GL_QUAD_STRIP);
		glVertex3f(-0.5,0.5,-4); // 4
		glVertex3f(-0.5,-0.5,-4); // 1
		glVertex3f(0.5,0.5,-4); // 3
		glVertex3f(0.5,-0.5,-4); // 2
	glEnd(); 

	glColor3f(0,1,0);
	glBegin(GL_QUAD_STRIP);
		glVertex3f(0.5,0.5,-4); // 3
		glVertex3f(0.5,-0.5,-4); // 2
		glVertex3f(0.5,0.5,-5); // 7
		glVertex3f(0.5,-0.5,-5); // 5
	glEnd();

	glColor3f(1,0,0);
	glBegin(GL_QUAD_STRIP);
		glVertex3f(0.5,0.5,-5); // 7
		glVertex3f(0.5,-0.5,-5); // 5
		glVertex3f(-0.5,0.5,-5); // 8
		glVertex3f(-0.5,-0.5,-5); // 6
	glEnd();

	glColor3f(0,1,0);
	glBegin(GL_QUAD_STRIP);
		glVertex3f(-0.5,0.5,-5); // 8
		glVertex3f(-0.5,-0.5,-5); // 6
		glVertex3f(-0.5,0.5,-4); // 4
		glVertex3f(-0.5,-0.5,-4); // 1
	glEnd();

	glColor3f(0,0,1);
	glBegin(GL_QUAD_STRIP);
		glVertex3f(-0.5,0.5,-5); // 8
		glVertex3f(-0.5,0.5,-4); // 4
		glVertex3f(0.5,0.5,-5); // 7
		glVertex3f(0.5,0.5,-4); // 3
	glEnd();

	glColor3f(0,0,1);
	glBegin(GL_QUAD_STRIP);
		glVertex3f(-0.5,-0.5,-5); // 6	
		glVertex3f(0.5,-0.5,-5); // 5
		glVertex3f(-0.5,-0.5,-4); // 1
		glVertex3f(0.5,-0.5,-4); // 2	
	glEnd();
	// glFlush();
}

void display() {
	
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
    
    // inverti l'ordine
	glTranslatef(0,0,-4.5);

	glRotatef(y_angle,1,0,0);
	glRotatef(x_angle,0,1,0);

	glTranslatef(0,0,+4.5);

	draw_box();
	glutSwapBuffers();
}

bool init()
{
	glClearColor(0.93f, 0.93f, 0.93f, 0.0f);
        glEnable(GL_CULL_FACE);
        glEnable(GL_DEPTH_TEST);   

	return true;
}

void mouse_button(int button, int state, int x, int y) {
	if(button == GLUT_LEFT_BUTTON && state == GLUT_DOWN) {
		press = true;
		xdiff = x - x_angle;
		ydiff = y - y_angle;
	}
	else 
		press = false;
}

void mouse_move(int x, int y) {
	if(press == true) {
		x_angle = x - xdiff;
		y_angle = y - ydiff;
		glutPostRedisplay(); // chiama la display se mouse premuto
	}	

	
}

void reshape(int w, int h) {
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	glViewport(0, 0, w, h);

	gluPerspective(45.0f, 1.0f * w / h, 1.0f, 100.0f); 
}

int main(int argc,char** argv) {
	glutInit(&argc,argv);
	glutInitDisplayMode(GLUT_RGB | GLUT_DEPTH| GLUT_DOUBLE);
	glutInitWindowSize(600,600);
	glutInitWindowPosition(100,200);
	glutCreateWindow("Cubo in movimento");
	glutMouseFunc(mouse_button);
	glutMotionFunc(mouse_move);
	glutDisplayFunc(display);
	glutReshapeFunc(reshape);

	if(!init())
		return 1;

	glutMainLoop();
}