#include <GL/glut.h>
#include <GL/glu.h>
#include <GL/gl.h>
#include <math.h>

void display() {

	glShadeModel(GL_FLAT);

	glClearColor(0.0f,0.0f,0.0f,1.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glColor3f(0,1,0);
	glBegin(GL_QUADS); 
		glVertex2f(-0.5,0.6); // v1
		glVertex2f(-0.5,-0.2); // v2
		glVertex2f(-0.2,-0.2); //v3
		glVertex2f(-0.2,0.6); // v4
	glEnd();

	glColor3f(1,1,1);
	glBegin(GL_QUADS);
		glVertex2f(-0.2,0.6); // 4
		glVertex2f(-0.2,-0.2); //
		glVertex2f(0.1,-0.2); // 5
		glVertex2f(0.1,0.6); // 6
	glEnd();

	glColor3f(1,0,0);
	glBegin(GL_QUADS);
		glVertex2f(0.1,0.6);
		glVertex2f(0.1,-0.2);
		glVertex2f(0.4,-0.2); // 7
		glVertex2f(0.4,0.6); // 8
	glEnd();

	// Cerchio

	GLfloat radius = 0.3;
	GLfloat pi = 3.1415;
	glColor3f(0,0,1);
	glBegin(GL_TRIANGLE_FAN);
		glVertex2f(0.0,-0.6);
		for(float i=0; i<=2*pi; i=i+0.01) {
			glVertex2f(radius*cos(i), -0.6 + radius*sin(i));
		}
	glEnd();

	glFlush();
}

int main(int argc,char** argv) {
	glutInit(&argc,argv);
	glutInitDisplayMode(GLUT_RGB);
	glutInitWindowSize(500,500);
	glutInitWindowPosition(100,200);
	glutCreateWindow("Bandiera e cerchio");
	glutDisplayFunc(display);
	glutMainLoop();
}