#include <GL/glut.h>
#include <GL/glu.h>
#include <GL/gl.h>
#include <stdio.h>
#include <iostream>

#define MANO 1
#define POLSO 2
#define DITOSX 3
#define DITODX 4


GLfloat zrot = 0;
GLfloat yrot = 0;
GLfloat rotSX = 0;
GLfloat rotDX = 0;




void draw_cubes(){
	glPushMatrix();
	
		glInitNames();
		glPushName(0);
		glLoadName(MANO);
		

		// polso
		glPushMatrix();
		glColor3ub(255,255,255);
		glScalef(3,1,2);
		glPushName(POLSO);
		glutSolidCube(4.0f); // wire o solid
		glPopName();
		glPopMatrix();

		// dito sx
		glPushMatrix();
		glColor3ub(255,255,255);
		glTranslatef(-3,0,4);
		glRotatef(rotSX,1,0,0);
		glTranslatef(0,0,4);
		glScalef(0.9,0.7,2);
		glPushName(DITOSX);
		glutSolidCube(4.0f);
		glPopName();
		glPopMatrix();

		// dito dx
		glPushMatrix();
		glColor3ub(255,255,255);
		glTranslatef(+3,0,4);
		glRotatef(rotDX,1,0,0);
		glTranslatef(0,0,4);
		glScalef(0.9,0.7,2);
		glLoadName(DITODX);
		glutSolidCube(4.0f);
		glPopName();
		glPopName();
	glPopMatrix();



	glColor3f(1,0,0);
	glBegin(GL_LINES);
		glVertex3f(0,0,0);
		glVertex3f(2,0,0);
	glEnd();

	glColor3f(0,1,0);
	glBegin(GL_LINES);
		glVertex3f(0,0,0);
		glVertex3f(0,2,0);
	glEnd();

	glColor3f(0,0,1);
	glBegin(GL_LINES);
		glVertex3f(0,0,0);
		glVertex3f(0,0,2);
	glEnd();
	glPopMatrix();
	
}



void display(){
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);

	glLoadIdentity();

	glTranslatef(-2,0,0);

	glRotatef(zrot,0,0,1);
	glRotatef(yrot,0,1,0);

	glTranslatef(2,0,0);

	
	draw_cubes();
	glutSwapBuffers(); // al posto di flush() se usi double
}

void reshape(int w, int h) {
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glViewport(0,0,w,h);
	gluPerspective(45.0f,(float)w/h,1.0f,100.0f); 
	glTranslatef(0,0,-30.0f); // valore negativo per allontanarmi (solo nella reshape)
}


void stampaNome(int numero){
	switch(numero){
		case MANO:
			std::cout<<" MANO " << std::endl;
			break;
		case POLSO:
			std::cout<<" POLSO " << std::endl;
			break;
		case DITODX:
			std::cout<<" DITODX "<< std::endl;
			break; 
		case DITOSX:
			std::cout<<" DITOSX "<< std::endl;
			break;
		default:
		std::cout<<" **"<< numero << std::endl;
			break;

	}
}

void keyboard(unsigned char k, int x, int y){
	switch(k){
		case 'z':
			zrot = zrot + 0.45;
			glutPostRedisplay();
			break;
		case 'y':
			yrot = yrot + 0.45;
			glutPostRedisplay();
			break;	
		case 'a':
			rotSX = rotSX + 0.45;
			if (rotSX > 60) rotSX = (60);
			glutPostRedisplay();
			break;
		case 'q':
			rotSX = rotSX - 0.45;
			if (rotSX < -30) rotSX = (-30);	
			glutPostRedisplay();
			break;
		case 's':
			rotDX = rotDX + 0.45;
			if (rotDX > 60) rotDX = (60);
			glutPostRedisplay();
			break;
		case 'w':
			rotDX = rotDX - 0.45;
			if (rotDX < -30) rotDX = (-30);	
			glutPostRedisplay();
			break;			
	}
	glutPostRedisplay();
}

void processEnd(GLuint *selectBuff){
	int count = selectBuff[0];
	std::cout<<"Count:" << count << std::endl;
	int ris = 0;
	std::cout<<"Hai selezionato:" << std::endl;
	for (int i = 0; i < count; i++){
		ris = selectBuff[3 + i];
		stampaNome(ris);
	}
}

void processSelection(int x, int y){
	// creo area di selezione
	GLuint selectBuff[64];
	GLfloat fAspect;
	GLint hits, viewport[4];
	glSelectBuffer(64, selectBuff);
	glGetIntegerv(GL_VIEWPORT, viewport);
	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
		glRenderMode(GL_SELECT);
		glLoadIdentity();
		gluPickMatrix(x, viewport[3] - y, 4,4, viewport);
		fAspect = (float)viewport[2] / (float)viewport[3];
		gluPerspective(45.0f, fAspect, 1.0, 425.0);
		display();


	hits = glRenderMode(GL_RENDER);
	std::cout<<"Hits " << hits << std::endl;
	if (hits >= 1)
		processEnd(selectBuff);
	else
		std::cout<<"Non hai cliccato." << std::endl;
		glMatrixMode(GL_PROJECTION);
	glPopMatrix();
}



void mouseCallback(int button, int state, int x, int y)
	{
	if(button == GLUT_LEFT_BUTTON && state == GLUT_DOWN)
		processSelection(x, y);
}

bool init()
{
	glClearColor(0.10f, 0.10f, 0.10f, 0.0f);
       // glEnable(GL_CULL_FACE); // per le superifici nascoste
        glEnable(GL_DEPTH_TEST);   
	return true;
}

int main(int argc,char** argv) {
	glutInit(&argc,argv);
	glutInitDisplayMode(GLUT_RGB | GLUT_DEPTH | GLUT_DOUBLE);
	glutInitWindowSize(600,600);
	glutInitWindowPosition(100,200);
	if(!init())
		return 1;
	glutCreateWindow("Mano");
	glutMouseFunc(mouseCallback);
	// glutMotionFunc(mouse_move);
	glutKeyboardFunc(keyboard);
	glutDisplayFunc(display);
	glutReshapeFunc(reshape);

	glutMainLoop();
}