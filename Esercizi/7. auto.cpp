#include<GL/glut.h>
#include<GL/glu.h>
#include<GL/gl.h>
#include<string.h>
#include<stdio.h> 
#include<math.h>

#define DL_WHEEL 2
#define DL_WHEEL_BOLT 3
#define DL_FINAL 4

int roty= 0;

void init (void)
{
   float angle;

   glNewList(DL_WHEEL_BOLT,GL_COMPILE);
   glBegin(GL_TRIANGLE_FAN);
    glColor3f (0.0, 0.0, 1.0);
    for (int i=0; i<=6 ; i++)
      {
       float angle=2*3.14*i/6;                    // disegna una vite della ruota
       glVertex2f(cos(angle)*0.1, sin(angle)*0.1); 
      }
   glEnd(); 
   glEndList();


   /* TODO: scrivere una Display List per disegnare una ruota */
   glNewList(DL_WHEEL,GL_COMPILE);
       glBegin(GL_TRIANGLE_FAN);
        glColor3f(0,1,0);
        for (int i=0; i<=60 ; i++){
          float angle=2*3.14*i/60;                    // disegna una vite della ruota
          glVertex2f(cos(angle)*0.5, sin(angle)*0.5); 
        }

       glEnd();
       glPushMatrix();
          glTranslatef(0,0.25,0.05);
          glCallList(DL_WHEEL_BOLT);
       glPopMatrix();

       glPushMatrix();
          glTranslatef(0,-0.25,0.05);
          glCallList(DL_WHEEL_BOLT);
       glPopMatrix();
   glEndList();


   glNewList(DL_FINAL,GL_COMPILE);
      glPushMatrix();
        glTranslatef(+1.5,-0.5,1.1);
        glCallList(DL_WHEEL);
      glPopMatrix();

      glPushMatrix();
        glTranslatef(+1.5,-0.5,-1.1);
        glRotatef(180,0,1,0);
        glCallList(DL_WHEEL);
      glPopMatrix();

      glPushMatrix();
        glTranslatef(-1.5,-0.5,-1.1);
        glRotatef(180,0,1,0);
        glCallList(DL_WHEEL);
      glPopMatrix();

      glPushMatrix();
        glTranslatef(-1.5,-0.5,1.1);
        glCallList(DL_WHEEL);
      glPopMatrix();
    glEndList();
  
   glShadeModel (GL_FLAT);
}

void draw_car_body()  // disegna il corpo della macchina
{
    glColor3f(1.0,0.0,0.0);

    glBegin(GL_QUADS);
      // Front Face
      
      glVertex3f(-2.0, -0.5,  1.0);
      glVertex3f( 2.0, -0.5,  1.0);
      glVertex3f( 2.0,  0.5,  1.0);
      glVertex3f(-2.0,  0.5,  1.0);
      // Back Face
      
      glVertex3f(-2.0, -0.5, -1.0);
      glVertex3f(-2.0,  0.5, -1.0);
      glVertex3f( 2.0,  0.5, -1.0);
      glVertex3f( 2.0, -0.5, -1.0);
      // Top Face
      
      glVertex3f(-2.0,  0.5, -1.0);
      glVertex3f(-2.0,  0.5,  1.0);
      glVertex3f( 2.0,  0.5,  1.0);
      glVertex3f( 2.0,  0.5, -1.0);
      // Bottom Face
      
      glVertex3f(-2.0, -0.5, -1.0);
      glVertex3f( 2.0, -0.5, -1.0);
      glVertex3f( 2.0, -0.5,  1.0);
      glVertex3f(-2.0, -0.5,  1.0);
      // Right face
      
      glVertex3f( 2.0, -0.5, -1.0);
      glVertex3f( 2.0,  0.5, -1.0);
      glVertex3f( 2.0,  0.5,  1.0);
      glVertex3f( 2.0, -0.5,  1.0);
      // Left Face
      
      glVertex3f(-2.0, -0.5, -1.0);
      glVertex3f(-2.0, -0.5,  1.0);
      glVertex3f(-2.0,  0.5,  1.0);
      glVertex3f(-2.0,  0.5, -1.0);

      // Cabin
      // Front Face
      
      glVertex3f(-0.5,  0.5,  1.0);
      glVertex3f( 0.5,  0.5,  1.0);
      glVertex3f( 0.5,  1.0,  1.0);
      glVertex3f(-0.5,  1.0,  1.0);
      // Back Face
      
      glVertex3f(-0.5,  0.5, -1.0);
      glVertex3f(-0.5,  1.0, -1.0);
      glVertex3f( 0.5,  1.0, -1.0);
      glVertex3f( 0.5,  0.5, -1.0);
      // Top Face
      
      glVertex3f(-0.5,  1.0, -1.0);
      glVertex3f(-0.5,  1.0,  1.0);
      glVertex3f( 0.5,  1.0,  1.0);
      glVertex3f( 0.5,  1.0, -1.0);

      // Right face
      
      glVertex3f( 0.5,  0.5, -1.0);
      glVertex3f( 0.5,  1.0, -1.0);
      glVertex3f( 0.5,  1.0,  1.0);
      glVertex3f( 0.5,  0.5,  1.0);
      // Left Face
      
      glVertex3f(-0.5,  0.5, -1.0);
      glVertex3f(-0.5,  0.5,  1.0);
      glVertex3f(-0.5,  1.0,  1.0);
      glVertex3f(-0.5,  1.0, -1.0);

    glEnd();

}



void RenderScene(void)
{
	// Clear the window with current clearing color
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// Save the matrix state and do the rotations
	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();

	// Translate the whole scene out and into view	
	glTranslatef(0.0f, 0.0f, -10.0f);	
        glRotatef ((GLfloat) roty, 0.0, 1.0, 0.0);

        draw_car_body();

        glCallList(DL_FINAL);

  	
        /* TODO: scrivere il codice per disegnare le 4 ruote */
        

	// Restore the matrix state
	glPopMatrix();	// Modelview matrix

	glutSwapBuffers();
}

void SetupRC()
{

	glEnable(GL_DEPTH_TEST);	// Hidden surface removal
	glPolygonMode(GL_FRONT_AND_BACK,GL_FILL);
        glClearColor(0.60f, 0.60f, 0.60f, 1.0f );
}



void ChangeSize(int w, int h)
{
	GLfloat fAspect;		// Screen aspect ratio

	// Prevent a divide by zero
	if(h == 0)
		h = 1;

	// Set Viewport to window dimensions
        glViewport(0, 0, w, h);

	// Calculate aspect ratio of the window
	fAspect = (GLfloat)w/(GLfloat)h;

	// Set the perspective coordinate system
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	gluPerspective(45.0f, fAspect, 1.0, 40.0);

	// Modelview matrix reset
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
}

void keyboard (unsigned char key, int x, int y)
{
   switch (key) {
   
     case 'a':
         roty = (roty + 5) % 360 ;
         glutPostRedisplay();
         break;
      case 's':
         roty = (roty - 5) % 360;
         glutPostRedisplay();
         break;
   }
}


int main(int argc, char* argv[])
{
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
	glutInitWindowSize(800,600);
	glutCreateWindow("Car");
        init ();
	glutReshapeFunc(ChangeSize);
        glutKeyboardFunc(keyboard);
	glutDisplayFunc(RenderScene);
	SetupRC();
	glutMainLoop();

	return 0;
}

