#include <GL/glut.h>
#include <GL/glu.h>
#include <GL/gl.h>

bool mov_pos = true;

GLfloat x1 = -0.1;
GLfloat x2 = 0.1;
GLfloat y1 = -0.1;
GLfloat y2 = 0.1;

void display() {
	glClearColor(0,0,0,1);
	glClear(GL_COLOR_BUFFER_BIT);

	glColor3f(1,0,0);
	glRectf(x1,y1,x2,y2);

	glutSwapBuffers();
}

void idle() {

	if(mov_pos){
		if(x2 + 0.002 > 1.0f)
			mov_pos = false;
		else {
			x2 = x2 + 0.002;
			x1 = x1 + 0.002;
		} 
	}
	else {
		if(x1 - 0.002 < -1.0)
			mov_pos = true;
		else {
			x2 = x2 - 0.002;
			x1 = x1 - 0.002;
		}
	}


	glutPostRedisplay();
}

int main(int argc,char** argv) {
	glutInit(&argc,argv);
	glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE);
	glutInitWindowSize(700,700);
	glutInitWindowPosition(100,200);
	glutCreateWindow("Animazione");
	glutIdleFunc(idle);
	glutDisplayFunc(display);
	glutMainLoop();
}