#include<GL/glut.h>
#include<GL/glu.h>
#include<GL/gl.h>

GLfloat globalAmbientLight[] = { 0.1f, 0.1f, 0.1f, 1.0f };
GLfloat componentAmbient0[] = { 0.2f, 0.2f, 0.2f, 1.0f };
GLfloat componentDiffuse0[] = { 0.7f, 0.7f, 0.7f, 1.0f };
GLfloat componentSpecular0[] = { 0.9f, 0.9f, 0.9f, 1.0f };
GLfloat posiz0[] = { 1.0f, 1.0f, 1.0f, 0.0f };
GLfloat  specref[] =  { 0.6f, 0.6f, 0.6f, 1.0f };

GLfloat rotX = 0.0f;
GLfloat rotSC = 0.0f;


void setupRC() // init
    {
    // Grayish background
    glClearColor(0,0,0,1);
   
    // Cull backs of polygons
    glCullFace(GL_BACK); // rimuove superfici dietro?
    glEnable(GL_CULL_FACE);
    glEnable(GL_DEPTH_TEST);

    glEnable(GL_LIGHTING);
    glLightModelfv(GL_LIGHT_MODEL_AMBIENT,globalAmbientLight);
	
	//glEnable(GL_COLOR_MATERIAL);
    //glColorMaterial(GL_FRONT, GL_AMBIENT_AND_DIFFUSE);
    //glMateriali(GL_FRONT, GL_SHININESS, 128);

    glEnable(GL_LIGHT0);
    glLightfv(GL_LIGHT0,GL_AMBIENT,componentAmbient0);
    glLightfv(GL_LIGHT0,GL_DIFFUSE,componentDiffuse0);
    glLightfv(GL_LIGHT0,GL_SPECULAR,componentSpecular0);
    glLightfv(GL_LIGHT0,GL_POSITION,posiz0);

    glEnable(GL_COLOR_MATERIAL);
   glColorMaterial(GL_FRONT, GL_AMBIENT_AND_DIFFUSE);
   glMaterialfv(GL_FRONT, GL_SPECULAR,specref); // luce riflessa bianca
   glMateriali(GL_FRONT, GL_SHININESS,30);

}



void drawWorld(void){
	glShadeModel(GL_SMOOTH);
	glRotatef(rotX,1,0,0);
	glPushMatrix();
	    glPushMatrix();
	    	glTranslatef(-0.75f,0,0);
	    	glRotatef(-90,1,0,0);
	    	glColor3f(1,0,0);
	    	glutSolidTorus(0.275f,0.85f,30,30);
	    glPopMatrix();

    	glPushMatrix();
	    	glTranslatef(0,-0.85,0);
	    	glTranslatef(-0.75f,0,0);
	    	glRotatef(-90,1,0,0);
	    	glColor3f(0,1,0);
	    	glutSolidCone(1.0f,2.0f,30,30);
    	glPopMatrix();
    glPopMatrix();
        glPushMatrix();
    	glTranslatef(+0.75f,0,0);
    	glColor3f(0,0,1);
    	glutSolidSphere(1.0f,30,30);
    glPopMatrix();
    }


void renderScene(void) //display 
{
    // Clear the window with current clearing color
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glPushMatrix();
    	glRotatef(rotSC,0,1,0);
    	glLightfv(GL_LIGHT0,GL_POSITION,posiz0); // riposiziona la luce rispetto agli oggetti
    	drawWorld();
    glPopMatrix();

    glutSwapBuffers();
}



void changeSize(int w, int h)
    {
    GLfloat fAspect;

    if(h == 0)
        h = 1;

    glViewport(0, 0, w, h);
        
    fAspect = (GLfloat)w / (GLfloat)h;

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
	
    // Set the clipping volume
    gluPerspective(35.0f, fAspect, 1.0f, 50.0f);
        
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();    
    glTranslatef(0.0f, 0.0f, -10.0f);
    }



void keyboard(unsigned char key, int x, int y){
   switch (key) {
      case 27:
         exit(0);
         break;
      case 'a':
         rotSC-=0.5;
         break;
      case 'd':
         rotSC+=0.5;
         break;
	  case 'w':
         rotX-=0.5;
         break;
      case 's':
         rotX+=0.5;
         break;
   }
   glutPostRedisplay();
}

int main(int argc, char* argv[])
    {
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
    glutInitWindowSize(800,600);
    glutCreateWindow("Esercizio 8");
    glutReshapeFunc(changeSize);
    glutDisplayFunc(renderScene);
    glutKeyboardFunc (keyboard);
    // glutTimerFunc(10, TimerFunction, 1);
    
    setupRC();
    glutMainLoop();

    return 0;
    }
