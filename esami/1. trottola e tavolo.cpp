#include<GL/glut.h>
#include<GL/glu.h>
#include<GL/gl.h>


GLfloat globalAmbientLight[] = { 0.3f, 0.3f, 0.3f, 1.0f };
GLfloat componentAmbient0[] = { 0.5f, 0.5f, 0.5f, 1.0f };
GLfloat componentDiffuse0[] = { 0.5f, 0.5f, 0.5f, 1.0f };
GLfloat componentSpecular0[] = { 0.9f, 0.9f, 0.9f, 1.0f };
GLfloat posiz0[] = { 1.0f, 0.2f, 0.0f, 0.0f };
GLfloat  specref[] =  { 0.4, 0.4f, 0.4f, 1.0f }; // la potenza della luce: valori alti più bianco

GLfloat rotSC = 0;
GLfloat rotTrot = 0;
GLfloat xtrot = 0;
GLfloat ztrot = 0;
GLfloat xtav = 0;
GLfloat ztav = 0;

#define GAMBA 1

void setupRC(){ // init
    // Grayish background
    glClearColor(0,0,0,1);
   
    // Cull backs of polygons
    glCullFace(GL_BACK); // rimuove superfici dietro?
    glEnable(GL_CULL_FACE);
    glEnable(GL_DEPTH_TEST);

    glEnable(GL_LIGHTING);
    glLightModelfv(GL_LIGHT_MODEL_AMBIENT,globalAmbientLight);
	
	glEnable(GL_COLOR_MATERIAL);
    glColorMaterial(GL_FRONT, GL_AMBIENT_AND_DIFFUSE);
    glMateriali(GL_FRONT, GL_SHININESS, 128);
    glEnable(GL_NORMALIZE);

    // la lista la mettiamo in init
    glNewList(GAMBA,GL_COMPILE);
	    glColor3f (0.4, 0.2, 0.0);
		glScalef (2.0, 12.0, 2.0);
	    glutSolidCube(1);
    glEndList();

    glEnable(GL_LIGHT0);
    glLightfv(GL_LIGHT0,GL_AMBIENT,componentAmbient0);
    glLightfv(GL_LIGHT0,GL_DIFFUSE,componentDiffuse0);
    glLightfv(GL_LIGHT0,GL_SPECULAR,componentSpecular0);
    glLightfv(GL_LIGHT0,GL_POSITION,posiz0);

  glEnable(GL_COLOR_MATERIAL);
   glColorMaterial(GL_FRONT, GL_AMBIENT_AND_DIFFUSE);
   glMaterialfv(GL_FRONT, GL_SPECULAR,specref); // luce riflessa bianca
   glMateriali(GL_FRONT, GL_SHININESS,30);

}

void drawWorld(void){
	glPushMatrix();
	glTranslatef(xtav,0,ztav);
		glPushMatrix();
			glColor3f(0,1,0);
			glTranslatef(xtrot,18,ztrot);
			glScalef(0.5,2,0.5);
			glRotatef(rotTrot,0,1,0);
			glutSolidCube(1);
		glPopMatrix();

		glPushMatrix();
			glColor3f(0,0,1);
			glTranslatef(xtrot,17,ztrot);		
			glRotatef(rotTrot,0,1,0);
			glRotatef(90,1,0,0);
			glutSolidCone(1,3,50,50);
		glPopMatrix();


		glPushMatrix();
			glPushMatrix();
				glTranslatef(-10,6,-10);
				glCallList(GAMBA);
			glPopMatrix();
			glPushMatrix();
				glTranslatef(10,6,10);
				glCallList(GAMBA);
			glPopMatrix();
			glPushMatrix();
				glTranslatef(-10,6,10);
				glCallList(GAMBA);
			glPopMatrix();
			glPushMatrix();
				glTranslatef(10,6,-10);
				glCallList(GAMBA);
			glPopMatrix();			

			glColor3f(0.4,0.2,0.0);
			glTranslatef(0,13.0f,0);
			glScalef(22.0f,2.0f,22.0f);
			glutSolidCube(1.0f);

		glPopMatrix();
	glPopMatrix();
		glPushMatrix();
			glRotatef(-90,1,0,0);
			glColor3f(0.5,0.5,0.5);
			glRectf(-80,-80,80,80);
		glPopMatrix();
	
}


void renderScene(void) //display 
{
    // Clear the window with current clearing color
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glPushMatrix();
    	glLightfv(GL_LIGHT0,GL_POSITION,posiz0); // riposiziona la luce rispetto agli oggetti
    	glRotatef(rotSC,0,1,0);
    	drawWorld();
    glPopMatrix();

    glutSwapBuffers();
}

void keyboard(unsigned char key, int x, int y){
   switch (key) {
      case 27:
         exit(0);
         break;
      case 'o':
         rotSC-=0.5;
         break;
      case 'p':
         rotSC+=0.5;
         break;
      case 'a':
      	xtrot-= 0.5;
      	if (xtrot < -11) xtrot = -11;
      	break;
      case 'd':
      	xtrot+= 0.5;
      	if (xtrot > 11) xtrot = 11;
      	break;
      case 'w':
      	ztrot-= 0.5;
      	if (ztrot < -11) ztrot = -11;
      	break;
      case 's':
      	ztrot+= 0.5;
      	if (ztrot > 11) ztrot = 11;
      	break;
      case 'g':
      	xtav-= 0.5;
      	if (xtav < -69) xtav = -69;
      	break;
      case 'j':
      	xtav+= 0.5;
      	if (xtav > 69) xtav = 69;
      	break;
      case 'y':
      	ztav-= 0.5;
      	if (ztav < -69) ztav = -69;
      	break;
      case 'h':
      	ztav+= 0.5;
      	if (ztav > 69) ztav = 69;
      	break;	

   }
   glutPostRedisplay();
}



void changeSize(int w, int h)
    {
    GLfloat fAspect;

    if(h == 0)
        h = 1;

    glViewport(0, 0, w, h);
        
    fAspect = (GLfloat)w / (GLfloat)h;

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
	
    // Set the clipping volume
    gluPerspective(45.0f, fAspect, 1.0f, 800.0f);
        
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();  

    glTranslatef(0.0f, -20.0f, -150.0f);
    }

void idleScene(){
	 rotTrot+=0.8f;
	 glutPostRedisplay();
}


int main(int argc, char* argv[]) {
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
    glutInitWindowSize(800,600);
    glutCreateWindow("OpenGL Blending and Transparency");
    glutReshapeFunc(changeSize);
    glutDisplayFunc(renderScene);
    glutKeyboardFunc(keyboard);
    // glutSpecialFunc(SpecialKeys);
    glutIdleFunc(idleScene);
    
    setupRC();
    glutMainLoop();

    return 0;
}
