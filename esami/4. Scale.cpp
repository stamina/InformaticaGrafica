#include<GL/glut.h>
#include<GL/glu.h>
#include<GL/gl.h>
#include<cmath>
#include<iostream>

GLfloat globalAmbientLight[] = { 0.3f, 0.3f, 0.3f, 1.0f };
GLfloat componentAmbient0[] = { 0.8f, 0.8f, 0.8f, 1.0f };
GLfloat componentDiffuse0[] = { 0.8f, 0.8f, 0.8f, 1.0f };
GLfloat componentSpecular0[] = { 1.0f, 1.0f, 1.0f, 1.0f };
GLfloat posiz0[] = { 0, -1, 1, 0 };
GLfloat  specref[] =  { 0.4, 0.4f, 0.4f, 1.0f }; // la potenza della luce: valori alti più bianco

bool sfera = true;

GLfloat rotX = 0.0f;
GLfloat rotY = 0.0f;

GLfloat xogg = 0;
GLfloat yogg;
GLint NumGradini = 10;
GLint NumOgg = 4;

#define GRADINO 1
#define CONO 2

void setupRC(){ // init
    // Grayish background
    glClearColor(0.2,0.2,0.2,1);
   
    // Cull backs of polygons
    // glCullFace(GL_BACK); // rimuove superfici dietro?
    // glEnable(GL_CULL_FACE);
    glEnable(GL_DEPTH_TEST);


    glEnable(GL_LIGHTING);
    glLightModelfv(GL_LIGHT_MODEL_AMBIENT,globalAmbientLight);
    
    glEnable(GL_COLOR_MATERIAL);
    glColorMaterial(GL_FRONT, GL_AMBIENT_AND_DIFFUSE);
    glMateriali(GL_FRONT, GL_SHININESS, 128);
    glEnable(GL_NORMALIZE);

    glLightModeli(GL_LIGHT_MODEL_LOCAL_VIEWER, GL_TRUE);

    glEnable(GL_LIGHT0);
    glLightfv(GL_LIGHT0,GL_AMBIENT,componentAmbient0);
    glLightfv(GL_LIGHT0,GL_DIFFUSE,componentDiffuse0);
    glLightfv(GL_LIGHT0,GL_SPECULAR,componentSpecular0);
    // glLightfv(GL_LIGHT0,GL_POSITION,posiz0);

  glEnable(GL_COLOR_MATERIAL);
   glColorMaterial(GL_FRONT, GL_AMBIENT_AND_DIFFUSE);
   glMaterialfv(GL_FRONT, GL_SPECULAR,specref); // luce riflessa bianca
   glMateriali(GL_FRONT, GL_SHININESS,30);

	glNewList(GRADINO,GL_COMPILE);
		glColor3f(1,0,0);
		glPushMatrix();
			glBegin(GL_TRIANGLES);
				glVertex3f(5,0,-30); // v1
				glVertex3f(-5,0,-30); // v2
				glVertex3f(0,-8,-30); // v3
			glEnd();
			glBegin(GL_TRIANGLES);
				glVertex3f(-5,0,30); // v4
				glVertex3f(0,-8,30); // v5
				glVertex3f(5,0,30); // v6
			glEnd();

			glBegin(GL_QUAD_STRIP);
				glVertex3f(-5,0,-30); // v2
				glVertex3f(-5,0,30); // v4
				glVertex3f(5,0,-30); // v1
				glVertex3f(5,0,30); // v6
				glVertex3f(0,-8,-30); // v3
				glVertex3f(0,-8,30); // v5
				glVertex3f(-5,0,-30); // v2
				glVertex3f(-5,0,30); // v4
			glEnd();

		glPopMatrix();
    glEndList();

    glNewList(CONO,GL_COMPILE);
    	glPushMatrix();
    	glTranslatef(0,5,0);
    	glPushMatrix();
	    	glBegin(GL_TRIANGLE_FAN);
	    		glVertex3f(0,0,0);
	    		for (GLfloat angle = 0; angle < 360; angle = angle + 5){
	    			glVertex3f(5 * sin(angle),0,-cos(angle) *5 );
	    		}
	    	glEnd();
	    	
	  		glBegin(GL_TRIANGLE_FAN);
	    		glVertex3f(0,-5,0);
	    		for (GLfloat angle = 0; angle < 360; angle = angle + 5){
	    			glVertex3f(sin(angle)*5,0,-cos(angle)*5);
	    		}
	    	glEnd();
	    glPopMatrix();
	    glPopMatrix();
    glEndList();

}



void drawWorld(){	
	// disegna i gradini
	
	glPushMatrix();
		for (int i = 0; i< NumGradini; i++){
			glCallList(GRADINO);
			glTranslatef(20,-20,0);
		}
	glPopMatrix();

	// disegna oggetto
	glColor3f(0,1,0);
	glPushMatrix();
		if (sfera == true) glTranslatef(-0,5,-30);
		else glTranslatef(0,0,-30);
		glTranslatef(xogg,yogg,0);
		for (int i = 1; i <= NumOgg;i++){
			glTranslatef(0,0,60/(NumOgg+1));
			if (sfera == true)
				glutSolidSphere(5,50,50);
			else
				glCallList(CONO);
		}
	glPopMatrix();
	
	glEnable(GL_NORMALIZE);
}

/*
void caricaListe(){
	 // glDeleteLists(COLLINA,1);
	// nuova lista
}*/

void renderScene(void) //display 
{
    // Clear the window with current clearing color
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    // caricaListe();
    glShadeModel(GL_SMOOTH);
    glPushMatrix();  
        glRotatef(rotX,1,0,0);
        glRotatef(rotY,0,1,0);
        glLightfv(GL_LIGHT0,GL_POSITION,posiz0);
        glPushMatrix();
        	glTranslatef(-(NumGradini -1)*20/2,90,0);
    		drawWorld();
    	glPopMatrix();
    glPopMatrix();

    glutSwapBuffers();
}

void keyboard(unsigned char key, int x, int y){
   switch (key) {
      case 27:
         exit(0);
         break;
      case 'a':
         rotY-=2;
         break;
      case 'd':
         rotY+=2;
         break;
    case 'w':
         rotX-= 2;
        break;
    case 's':
         rotX+= 2;
        break;
    case 'c':
    	sfera = !sfera;
    	break;
    case 'z':
    	 if(NumOgg >= 1 && NumOgg <= 4){
	         NumGradini+=1;
	         NumOgg+=1;
     	}
         break;
    case 'x':
    	 if(NumOgg >= 2 && NumOgg <= 5){
	         NumGradini-=1;
	         NumOgg-=1;
     	 }

         break;
   }
   glutPostRedisplay();
}



void changeSize(int w, int h) {
    GLfloat fAspect;

    if(h == 0)
        h = 1;

    glViewport(0, 0, w, h);
        
    fAspect = (GLfloat)w / (GLfloat)h;

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
	
    // Set the clipping volume
    gluPerspective(45.0f, fAspect, 1.0f, 800.0f);
        
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();  
    glTranslatef(0,0, -300.0f);
    }


GLfloat funzione(GLfloat x){
	int num = floor(x / 20);
	x = x - num*20;
	return (-(3.0/10)*pow(x,2) + 5* x - num*20);
}

void idleScene(){
	// cosa fa ogni volta? incrementa x, e se supera 20 torna a 0
	xogg = xogg + 0.2;
	if (xogg > (NumGradini-1)*20) xogg = 0;
	yogg = funzione(xogg);
	std::cout << xogg << " - " << yogg << std::endl;
	glutPostRedisplay();
}

int main(int argc, char* argv[]) {
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
    glutInitWindowSize(800,600);
    glutCreateWindow("Scala");
    glutReshapeFunc(changeSize);
    glutDisplayFunc(renderScene);
    glutKeyboardFunc(keyboard);
    // glutSpecialFunc(SpecialKeys);
    glutIdleFunc(idleScene);
    
    setupRC();
    glutMainLoop();

    return 0;
}
