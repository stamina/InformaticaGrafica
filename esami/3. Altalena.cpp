#include <GL/glut.h>
#include <GL/glu.h>
#include <GL/gl.h>
#include <iostream>
#include <cmath>
#include <climits>

GLfloat globalAmbientLight[] = { 0.1f, 0.1f, 0.1f, 1.0f };
GLfloat componentAmbient0[] = { 0.3f, 0.3f, 0.3f, 1.0f };
GLfloat componentDiffuse0[] = { 0.8f, 0.8f, 0.8f, 1.0f };
GLfloat componentSpecular0[] = { 1.0f, 1.0f, 1.0f, 1.0f };
GLfloat posiz0[] = { 80, 80, 80, 1 };
GLfloat  specref[] =  { 0.4, 0.4f, 0.4f, 1.0f }; // la potenza della luce: valori alti più bianco

bool animated = true;

GLfloat rotX = 0.0f;
GLfloat rotY = 0.0f;
GLfloat alfa = 0.01;
GLfloat angoloAltalena = 0.0f;
GLfloat incremento = 0.05;

#define CATENA 1
#define COLLINA 2
#define SCENA 3
#define ASSE 4
#define SEGGIOLINO 5

void setupRC(){ // init
    // Grayish background
    glClearColor(0.2,0.45,0.45,1);
   
    // Cull backs of polygons
    //glCullFace(GL_BACK); // rimuove superfici dietro?
   // glEnable(GL_CULL_FACE);
    glEnable(GL_DEPTH_TEST);


    glEnable(GL_LIGHTING);
    glLightModelfv(GL_LIGHT_MODEL_AMBIENT,globalAmbientLight);
    
    glEnable(GL_COLOR_MATERIAL);
    glColorMaterial(GL_FRONT, GL_AMBIENT_AND_DIFFUSE);
    glMateriali(GL_FRONT, GL_SHININESS, 128);
    glEnable(GL_NORMALIZE);

    glLightModeli(GL_LIGHT_MODEL_LOCAL_VIEWER, GL_TRUE);

    glEnable(GL_LIGHT0);
    glLightfv(GL_LIGHT0,GL_AMBIENT,componentAmbient0);
    glLightfv(GL_LIGHT0,GL_DIFFUSE,componentDiffuse0);
    glLightfv(GL_LIGHT0,GL_SPECULAR,componentSpecular0);
//     glLightfv(GL_LIGHT0,GL_POSITION,posiz0);

  glEnable(GL_COLOR_MATERIAL);
   glColorMaterial(GL_FRONT, GL_AMBIENT_AND_DIFFUSE);
   glMaterialfv(GL_FRONT, GL_SPECULAR,specref); // luce riflessa bianca
   glMateriali(GL_FRONT, GL_SHININESS,30);

	glNewList(CATENA,GL_COMPILE);
		glColor3f(0.5,0.5,0.5);
		for (int conteggio = 0; conteggio < 10; conteggio++){
			glPushMatrix();
				glTranslatef(0,-conteggio*3,0);
				if (conteggio % 2 == 0)
					glRotatef(90,0,1,0);
	        	glutSolidTorus(0.5,2,50,50);
	        glPopMatrix();
    	}
    glEndList();

}



void drawWorld(){
	// collina
	
	glInitNames();
	glPushName(0);

	glLoadName(SCENA);
	glPushMatrix();
		glPushMatrix();
			glPushName(COLLINA);
			glCallList(COLLINA);
		glPopMatrix();

		glColor3f(0.5,0.2,0);
		// asse sx
		glPushMatrix();
			glLoadName(ASSE);
			glTranslatef(0,-alfa*(pow(-19,2)+pow(0,2)),0);
			glTranslatef(-17,25,0);
			glScalef(4,50,4);
			glutSolidCube(1);
		glPopMatrix();

		// asse dx
		glPushMatrix();
			glLoadName(ASSE);
			glTranslatef(0,-alfa*(pow(19,2)+pow(0,2)),0);
			glTranslatef(17,25,0);
			glScalef(4,50,4);
			glutSolidCube(1);
		glPopMatrix();

		// linea
		glPushMatrix();
			glLoadName(ASSE);
			glTranslatef(0,-alfa*(pow(19,2)+pow(0,2)) + 48,0);
			glBegin(GL_LINES);
				glColor3f(0.7,0.7,0.7);
				glVertex3f(-16,0,0);
				glVertex3f(16,0,0);
			glEnd();
		glPopMatrix();

		glPushMatrix();
			glTranslatef(0,(-alfa*(pow(19,2)+pow(0,2)) + 46.5),0);
			glRotatef(sin(angoloAltalena)*40,1,0,0);
			glTranslatef(0,-(-alfa*(pow(19,2)+pow(0,2)) + 46.5),0);
			// catena sx
			glPushMatrix();
				glLoadName(CATENA);
				glTranslatef(-8,-alfa*(pow(19,2)+pow(0,2)) + 46.5,0);
				glCallList(CATENA);
			glPopMatrix();

			// catena dx
			glPushMatrix();
				glLoadName(CATENA);
				glTranslatef(8,-alfa*(pow(19,2)+pow(0,2)) + 46.5,0);
				glCallList(CATENA);
			glPopMatrix();

			// seggiolino
			glPushMatrix();
				glLoadName(SEGGIOLINO);
				glColor3f(0.2,0.2,0.2);
				glTranslatef(0,-alfa*(pow(19,2)+pow(0,2)) + 46.5 - 30.5,0);
				glScalef(16,2,5);
				glutSolidCube(1);
			glPopMatrix();

			// cono
			glPushMatrix();
				glLoadName(SEGGIOLINO);
				glColor3f(1,1,0);
				glTranslatef(0,-alfa*(pow(19,2)+pow(0,2)) + 46.5 - 30.5 + 1,0);
				glRotatef(-90,1,0,0);
				glutSolidCone(2.5,8,50,50);
			glPopMatrix();
		glPopMatrix();

	glPopMatrix();
	glPopName();
	glPopName();
	
	glEnable(GL_NORMALIZE);
}


void caricaListe(){
	glDeleteLists(COLLINA,1);

    glNewList(COLLINA,GL_COMPILE);
    	glColor3f(0,0.5,0.0);
    	for (float x = -50; x < 50; x = x+0.5){
    		for (float z = -50; z < 50; z = z+0.5){
    			glBegin(GL_QUADS);
    				glVertex3f(x,-alfa*(pow(x,2)+pow(z,2)),z);
    				glVertex3f(x,-alfa*(pow(x,2)+pow(z+0.5,2)),z+0.5);
    				glVertex3f(x+0.5,-alfa*(pow(x+0.5,2)+pow(z+0.5,2)),z+0.5);
    				glVertex3f(x+0.5,-alfa*(pow(x+0.5,2)+pow(z,2)),z);
    			glEnd();
    		}
    	}
    glEndList();
}

void renderScene(void) //display 
{
    // Clear the window with current clearing color
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glMatrixMode(GL_MODELVIEW);
    caricaListe();


    glShadeModel(GL_SMOOTH);
    glPushMatrix();  
    glLoadIdentity();  
    glTranslatef(0.0f, 0.0f, -200.0f);
    
        glRotatef(rotX,1,0,0);
        glRotatef(rotY,0,1,0);
        glLightfv(GL_LIGHT0,GL_POSITION,posiz0);
    	drawWorld();
    glPopMatrix();

    glutSwapBuffers();
}

void keyboard(unsigned char key, int x, int y){
   switch (key) {
      case 27:
         exit(0);
         break;
      case 'z':
         rotY-=2;
         break;
      case 'x':
         rotY+=2;
         break;
    case 'v':
         rotX-= 2;
        break;
    case 'c':
         rotX+= 2;
        break;
    case '+':
         alfa+=0.0001;
         break;
    case '-':
         alfa-=0.0001;
         break;
   }
   glutPostRedisplay();
}



void Process(GLuint *pSelectBuff){
	int ogg = pSelectBuff[3];
	std::cout << pSelectBuff[0] << " -- " << pSelectBuff[1] << " -- " << pSelectBuff[2] << " -- " << pSelectBuff[3] << " -- " << std::endl;
	if (ogg == SCENA){
		animated = !animated;
		std::cout<< "click" << animated << std::endl;
	}
	
}


void process_zObject(GLuint *selectBuffer, GLint hits) {
	int id = 0;
	GLuint z_min = UINT_MAX; // Sparo al massimo la z
	GLuint id_min = INT_MIN; // Sparo al minimo l'id

	for(int i=0; i<hits; i++) {
		for(int j=id; j<=(id+2+selectBuffer[id]); j++) {
			std::cout << selectBuffer[j] << std::endl;
		}

		std::cout << std::endl;
		// Se il valore di z nel buffer
		if(selectBuffer[id+1] < z_min) {
			// Prendere il valore della z_min nel buffer
			z_min = selectBuffer[id+1];
			id_min = selectBuffer[id+2+selectBuffer[id]];
		}

		id += selectBuffer[id]+3;
	}

	std::cout << "Selected object: " << id_min << std::endl;

	glutPostRedisplay();
}

#define BUFFER_LENGTH 64
void processSelection(int xPos, int yPos){
	GLfloat fAspect;	// Screen aspect ratio

	// Space for selection buffer
	GLuint selectBuff[BUFFER_LENGTH];

	// Hit counter and viewport storeage
	GLint viewport[4];

	// Setup selection buffer
	glSelectBuffer(BUFFER_LENGTH, selectBuff);
	
	// Get the viewport
	glGetIntegerv(GL_VIEWPORT, viewport);

	// Switch to projection and save the matrix
	glMatrixMode(GL_PROJECTION);
	glPushMatrix();

	// Change render mode
	glRenderMode(GL_SELECT);

	// Establish new clipping volume to be unit cube around
	// mouse cursor point (xPos, yPos) and extending two pixels
	// in the vertical and horzontal direction. Remember OpenGL specifies the
	// y coordinate from the bottom, Windows from the top. So windows position
	// (as measured from the top) subtract the height and you get it in terms 
	// OpenGL Likes.
	glLoadIdentity();
	gluPickMatrix(xPos, viewport[3] - yPos, 2,2, viewport);

	// Apply perspective matrix 
	fAspect = (float)viewport[2] / (float)viewport[3];
	gluPerspective(45.0f, fAspect, 1.0f, 20000.0f);

	// Draw the scene
	renderScene();
	
	GLint hits = 0;
	hits = glRenderMode(GL_RENDER);
	std::cout << "Conteggio di hits: " << hits << std::endl;
	// If a single hit occured, display the info.
	if(hits >= 1){
		process_zObject(selectBuff,hits);
		// Process(selectBuff);
	}

 	// Restore the projection matrix
	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	// Go back to modelview for normal rendering
	// glMatrixMode(GL_MODELVIEW);

	// Collect the hits
	
}

void changeSize(int w, int h) {
    GLfloat fAspect;

    if(h == 0)
        h = 1;

    glViewport(0, 0, w, h);
        
    fAspect = (GLfloat)w / (GLfloat)h;

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
	
    // Set the clipping volume
    gluPerspective(45.0f, fAspect, 1.0f, 20000.0f);
        
    glMatrixMode(GL_MODELVIEW);
   
    }

void mouse( int but, int state, int x, int y ){
	if (but == GLUT_LEFT_BUTTON && state == GLUT_DOWN)
		processSelection(x,y);
}


void idleScene(){
	if ((angoloAltalena > 45 || angoloAltalena < -45) && animated == true)
	 		incremento = incremento * (-1);
	angoloAltalena = angoloAltalena + incremento;
	 glutPostRedisplay();
}

int main(int argc, char* argv[]) {
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
    glutInitWindowSize(800,600);
    glutCreateWindow("Altalena");
    glutReshapeFunc(changeSize);
    glutDisplayFunc(renderScene);
    glutKeyboardFunc(keyboard);
    glutMouseFunc(mouse);
    // glutSpecialFunc(SpecialKeys);
    glutIdleFunc(idleScene);
    
    setupRC();
    glutMainLoop();

    return 0;
}
