#include<GL/glut.h>
#include<GL/glu.h>
#include<GL/gl.h>
#include<cmath>
#include <iostream>

static GLfloat xRot = 0.0f;
static GLfloat yRot = 0.0f;

GLfloat xHighCenter = 0;
GLfloat yHighCenter;

GLfloat angoloGiostra = 0;
GLfloat rotazCilindro = 0;
int incremento = 2;
GLfloat raggio = 5;

// Light parameters

GLfloat global_ambient[] = {0.3f,0.3f,0.3f,1.0f};

GLfloat dir_ambient[] = {0.8f,0.8f,0.8f,1.0f};
GLfloat dir_diffuse[] = {0.8f,0.8f,0.8f,1.0f};
GLfloat dir_specular[] = {1.0f,1.0f,1.0f,1.0f};

GLfloat dir_position[] = {0.0f,1.0f,-500.0f,0.0f};


#define BASE 1
#define OMINO 2
#define CILINDRO 3       

int nOmini = 6;
bool stop = false;

double toRadian(double deg) {
    return deg * M_PI / 180.0;
}


void SetupRC() // init
    {
    // Grayish background
    glClearColor(0.2,0.2,0.2,1);
   
    // Cull backs of polygons
   // glCullFace(GL_BACK); // rimuove superfici dietro?
   //  glEnable(GL_CULL_FACE);
    glEnable(GL_DEPTH_TEST);
    glLightModeli(GL_LIGHT_MODEL_LOCAL_VIEWER, GL_TRUE);
    
    // Setup light parameters
    glLightModelfv(GL_LIGHT_MODEL_AMBIENT, global_ambient); // no luce ambientale globale
    glLightfv(GL_LIGHT0, GL_AMBIENT, dir_ambient);
    glLightfv(GL_LIGHT0, GL_DIFFUSE, dir_diffuse);
    glLightfv(GL_LIGHT0, GL_SPECULAR, dir_specular);
    glLightfv(GL_LIGHT0,GL_POSITION, dir_position);
    glEnable(GL_LIGHTING);
    glEnable(GL_LIGHT0);
     
    // Mostly use material tracking
    glEnable(GL_COLOR_MATERIAL);
    glColorMaterial(GL_FRONT, GL_AMBIENT_AND_DIFFUSE);
    glMateriali(GL_FRONT, GL_SHININESS, 128);


    glNewList(OMINO,GL_COMPILE);
        glScalef(0.8,0.8,0.8);
        glColor3f(1,1,0);
        glPushMatrix();
            glRotatef(-90,1,0,0);
            glutSolidCone(1,2,50,50);
        glPopMatrix();
        glColor3f(1,0,0);
        glTranslatef(0,2.5,0);
        glutSolidSphere(0.5,50,50);
    glEndList();


    glNewList(BASE,GL_COMPILE);
        glColor3f(0.0f, 1.0f, 0.0f);
        glBegin(GL_QUADS);
            glVertex3f(25,0,2.5); // v1
            glVertex3f(-25,0,2.5); // v2
            glVertex3f(-25,0,-2.5); // v3
            glVertex3f(25,0,-2.5); // v4
        glEnd();

        glBegin(GL_QUADS);
            glVertex3f(-25,0,-2.5); // v3
            glVertex3f(-25,0,2.5); // v2
            glVertex3f(-25,30,2.5); // v5
            glVertex3f(-25,30,-2.5); // v6
        glEnd();

        glBegin(GL_QUADS);
            glVertex3f(25,0,2.5); // v1
            glVertex3f(25,0,-2.5); // v4
            glVertex3f(25,30,-2.5); // v8
            glVertex3f(25,30,2.5); // v7
        glEnd();

        glPushMatrix();
                GLfloat angolo = 180;
                yHighCenter = sin(toRadian(angolo+90))*25 + 30;
                for (angolo; angolo < 360; angolo = angolo + 0.2){
                    glPushMatrix();

                        glBegin(GL_POLYGON);
                        glVertex3f(cos(toRadian(angolo))*25, sin(toRadian(angolo))*25 + 30,2.5);
                        glVertex3f(cos(toRadian(angolo))*25,0,2.5);
                        glVertex3f(cos(toRadian(angolo + 0.2))*25,0,2.5);
                        glVertex3f(cos(toRadian(angolo + 0.2))*25, sin(toRadian(angolo + 0.2))*25 + 30,2.5);
                        glEnd();

                        glBegin(GL_POLYGON);
                        glVertex3f(cos(toRadian(angolo + 0.2))*25, sin(toRadian(angolo + 0.2))*25 + 30,-2.5);
                        glVertex3f(cos(toRadian(angolo + 0.2))*25,0,-2.5);
                        glVertex3f(cos(toRadian(angolo))*25,0,-2.5);
                        glVertex3f(cos(toRadian(angolo))*25, sin(toRadian(angolo))*25 + 30,-2.5);
                        glEnd();

                        glBegin(GL_QUAD_STRIP);
                        glVertex3f(cos(toRadian(angolo))*25, sin(toRadian(angolo))*25 + 30,-2.5);
                        glVertex3f(cos(toRadian(angolo))*25, sin(toRadian(angolo))*25 + 30,2.5);
                        glVertex3f(cos(toRadian(angolo + 0.2))*25, sin(toRadian(angolo + 0.2))*25 + 30,-2.5);
                        glVertex3f(cos(toRadian(angolo + 0.2))*25, sin(toRadian(angolo + 0.2))*25 + 30,2.5);
                        glEnd();

                        
                    glPopMatrix();
                }
        glPopMatrix();

    glEndList();
    }



void keyboard(unsigned char key, int x, int y){
   switch (key) {
      case 27:
         exit(0);
         break;
    case 'w':
         raggio = raggio + 0.5;
        if (raggio > 10) raggio = 10;
        break;
    case 's':
         raggio = raggio - 0.5;
        if (raggio < 3) raggio = 3;
        break;
    case 'p':
        nOmini++;
        if (nOmini > 10) nOmini = 10;
        break;
    case 'o':
        nOmini--;
        if (nOmini < 3) nOmini = 3;
        break;
    case 'c':
        stop = !stop;
        break;
   }
   glutPostRedisplay();
}

void SpecialKeys(int key, int x, int y) {
    if(key == GLUT_KEY_UP)
        xRot-= 5.0f;

    if(key == GLUT_KEY_DOWN)
        xRot += 5.0f;

    if(key == GLUT_KEY_LEFT)
        yRot -= 5.0f;

    if(key == GLUT_KEY_RIGHT)
        yRot += 5.0f;

    if(xRot > 356.0f)
        xRot = 0.0f;

    if(xRot < -1.0f)
        xRot = 355.0f;

    if(yRot > 356.0f)
        yRot = 0.0f;

    if(yRot < -1.0f)
        yRot = 355.0f;

    // Refresh the Window
    glutPostRedisplay();
    }

void DrawWorld(void)
    {
    glPushMatrix();  
        glCallList(BASE);
        glPushMatrix();
            glTranslatef(xHighCenter,yHighCenter,0);
            // tutto ciò che va traslato in alto
            glTranslatef(0,25,0);
            glRotatef(sin(toRadian(angoloGiostra))*85,0,0,1);
            glTranslatef(0,-25,0);
            glPushMatrix();
                glTranslatef(0,1.5,0);
                glutSolidCube(3);
            glPopMatrix();


            glPushMatrix();
                glRotatef(rotazCilindro,0,1,0);
                glPushMatrix();
                    glTranslatef(0,3,0);
                    glCallList(CILINDRO);
                glPopMatrix();
                glPushMatrix();
                 glTranslatef(0,4,0);
                 for (int i = 0; i < nOmini; i++){
                    glPushMatrix();
                        glTranslatef(cos(toRadian(360/nOmini)*i)*raggio*2/3,0,sin(toRadian(360/nOmini)*i)*raggio*2/3);
                        glCallList(OMINO);
                    glPopMatrix();

                 }
                 glPopMatrix();
                // glutSolidSphere(1,50,50);
            glPopMatrix();
        glPopMatrix();
    glPopMatrix();

    }

void caricaListe(){
        glDeleteLists(CILINDRO,1);
        glNewList(CILINDRO,GL_COMPILE);
            glBegin(GL_TRIANGLE_FAN);
            glColor3f(1,0,0);
            glVertex3f(0,0,0);
            for (GLfloat angolo = 0; angolo <= 360; angolo = angolo + 0.5){
                glVertex3f(cos(toRadian(angolo))*raggio,0,sin(toRadian(angolo))* raggio);
            }
            glEnd();

            glBegin(GL_TRIANGLE_FAN);
            glColor3f(1,0,0);
            glVertex3f(0,1,0);
            for (GLfloat angolo = 0; angolo <= 360; angolo = angolo + 0.5){
                glVertex3f(cos(toRadian(angolo))*raggio,1,sin(toRadian(angolo))* raggio);
            }
            glEnd();

            glBegin(GL_QUAD_STRIP);
            for (GLfloat angolo = 0; angolo <= 360; angolo = angolo + 0.5){
                glVertex3f(cos(toRadian(angolo))*raggio,1,sin(toRadian(angolo))* raggio);
                glVertex3f(cos(toRadian(angolo))*raggio,0,sin(toRadian(angolo))* raggio);
            }
            glEnd();
        glEndList();
}


void RenderScene(void)  // display
    {
        caricaListe();
    // Clear the window with current clearing color
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    // glLightfv(GL_LIGHT0,GL_POSITION, fLightPos);    
    glPushMatrix();
    //    glEnable(GL_LIGHTING);

        glRotatef(xRot, 1.0f, 0.0f, 0.0f);
        glRotatef(yRot, 0.0f, 1.0f, 0.0f);

        glPushMatrix();
            // Move light under floor to light the "reflected" world
            
            DrawWorld();
        glPopMatrix();
    glPopMatrix();
        
    glutSwapBuffers();
    }


void idleScene(){
    rotazCilindro = rotazCilindro + 10;

    // if (angoloGiostra > 85 || angoloGiostra < -85 ) incremento = -1 * incremento;
        if (!stop || (int)angoloGiostra % 360 != 0)
           angoloGiostra = angoloGiostra + incremento;

    glutPostRedisplay();
}

void ChangeSize(int w, int h)
    {
    GLfloat fAspect;

    if(h == 0)
        h = 1;

    glViewport(0, 0, w, h);
        
    fAspect = (GLfloat)w / (GLfloat)h;

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
	
    // Set the clipping volume
    gluPerspective(35.0f, fAspect, 1.0f, 800.0f);
        
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();    
    glTranslatef(0,-20, -100.0f);
    }

int main(int argc, char* argv[])
    {
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
    glutInitWindowSize(800,600);
    glutCreateWindow("OpenGL Blending and Transparency");
    glutReshapeFunc(ChangeSize);
    glutDisplayFunc(RenderScene);
    glutSpecialFunc(SpecialKeys);
    glutKeyboardFunc(keyboard);
    glutIdleFunc(idleScene);
    SetupRC();
    glutMainLoop();

    return 0;
    }
