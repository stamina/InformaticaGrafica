#include <GL/glut.h>
#include <GL/glu.h>
#include <GL/gl.h>
#include <stdlib.h>
#include <stdio.h>
#include <cmath>

#define PI 3.14159265
#define INGRANAGGIO 1

GLfloat angleIng = 0;
int incremento = 1;
GLfloat rotx=0.0;
GLfloat roty=0.0;
GLfloat velocita = 0.5;
bool light = true;

GLfloat  ambientLight[] = { 0.5f, 0.5f, 0.5f, 1.0f};

GLfloat light0_ambient[] = { 0.3, 0.3, 0.3, 1.0 };
GLfloat light0_diffuse[] = { 0.4, 0.4, 0.4, 1.0 };
GLfloat light0_specular[] = { 1.0, 1.0, 1.0, 1.0 };
GLfloat light0_position[] = { 1.0, 1.0, 1.0, 0.0 };
GLfloat  specref[] =  { 0.6f, 0.6f, 0.6f, 1.0f };



double toRadian(float grado){
  return (grado * PI) / 180;
}

void init (void){

   glLightModelfv(GL_LIGHT_MODEL_AMBIENT, ambientLight);

   glLightfv (GL_LIGHT0, GL_AMBIENT, light0_ambient);
   glLightfv (GL_LIGHT0, GL_DIFFUSE, light0_diffuse);
   glLightfv (GL_LIGHT0, GL_SPECULAR, light0_specular);
   glLightfv (GL_LIGHT0, GL_POSITION, light0_position);
   glEnable(GL_LIGHT0);

   //glEnable(GL_LIGHT1);
   
   glEnable(GL_COLOR_MATERIAL);
   glColorMaterial(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE);
   glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR,specref);
   glMateriali(GL_FRONT_AND_BACK, GL_SHININESS,30);
   

   //glEnable(GL_CULL_FACE);
   glEnable(GL_DEPTH_TEST);   
   glShadeModel(GL_SMOOTH);
   glEnable(GL_NORMALIZE);

   glNewList(INGRANAGGIO,GL_COMPILE);
       glBegin(GL_TRIANGLE_FAN);
       glVertex3f(0,0,10);
       for (float angolo = 0; angolo <= 360; angolo = angolo + 0.5){
          glVertex3f(cos(toRadian(angolo))*60,sin(toRadian(angolo))*60,10);
       }
       glEnd();

        glBegin(GL_TRIANGLE_FAN);
       glVertex3f(0,0,-10);
       for (float angolo = 0; angolo <= 360; angolo = angolo + 0.5){
          glVertex3f(cos(toRadian(angolo))*60,sin(toRadian(angolo))*60,-10);
       }
       glEnd();

       glBegin(GL_QUAD_STRIP);
       glVertex3f(0,0,-10);
       for (float angolo = 0; angolo <= 360; angolo = angolo + 0.5){     
          glVertex3f(cos(toRadian(angolo))*60,sin(toRadian(angolo))*60,10);
          glVertex3f(cos(toRadian(angolo))*60,sin(toRadian(angolo))*60,-10);
       }
       glEnd();

       for (float angolo = 0; angolo <= 360; angolo = angolo + 360/8){
          glPushMatrix();
          glRotatef(angolo,0,0,1);
          glTranslatef(0,57 + 60*PI/16,0);
          glScalef(60*PI*2/16,20,20);
          glutSolidCube(1);
          glPopMatrix();
       }

   glEndList();

}

void drawWorld(){
  glInitNames();
  glPushName(0);

  glPushMatrix();
    glPushMatrix();
      glLoadName(INGRANAGGIO);
      glColor3f(1,0,0);
      glRotatef(-angleIng,0,0,1);
      glCallList(INGRANAGGIO);
    glPopMatrix();


    glPushMatrix();
      glLoadName(INGRANAGGIO);
      glColor3f(0,1,0);
      glTranslatef(140,0,0);
      glRotatef(angleIng,0,0,1);
      glRotatef(22.5,0,0,1);
      glCallList(INGRANAGGIO);
    glPopMatrix();
  glPopMatrix();
}




void idle(){
  if (incremento > 0)
    angleIng = angleIng + velocita; 
  else
    angleIng = angleIng - velocita;
  glutPostRedisplay();
}

void display (void){
   glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

   if (light){
     glEnable (GL_LIGHTING);
     glEnable (GL_LIGHT0);
     ambientLight[4] = 1;
  }
  else {
    glDisable(GL_LIGHTING);
    glDisable(GL_LIGHT0);
    ambientLight[4] = 0;
  }

   glPushMatrix();
     glTranslatef(-30,30,-300);
     glRotatef(roty,0,1,0);
     glRotatef(rotx,1,0,0);
     glRotatef(-45,0,0,1);
     drawWorld();
   glPopMatrix();
   
   // se fosse single userei glFlush(), ma è double
   glutSwapBuffers();
}

void reshape(int w, int h){
   // Prevent a divide by zero
	if(h == 0)
		h = 1;

	// Set Viewport to window dimensions
    glViewport(0, 0, w, h);

	// Calculate aspect ratio of the window
	float fAspect = (GLfloat)w/(GLfloat)h;

	// Set the perspective coordinate system
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	gluPerspective(65.0, (GLfloat) w/(GLfloat) h, 1.0, 800.0);

   glMatrixMode (GL_MODELVIEW);
   glLoadIdentity ();

}

void keyboard(unsigned char key, int x, int y) {
   switch (key) {
      case 27:
         exit(0);
         break;
      case 'a':
         rotx+=5.0;
         break;
      case 's':
         rotx-=5.0;
         break;
	    case 'z':
         roty+=5.0;
         break;
      case 'x':
         roty-=5.0;
         break;
      case '+':
          velocita+=0.1;
          break;
      case '-':
          velocita-=0.1;
          break;          
      case 'l':
          light = !light;
          break; 
   }
   glutPostRedisplay();
 }

void ProcessScene(GLuint *pSelectBuff){
  int id,count;
  count = pSelectBuff[0];
  id = pSelectBuff[3];

  switch(id) {
    case INGRANAGGIO:
      incremento = - incremento;
      glutPostRedisplay();
      break;
    }
}



#define BUFFER_LENGTH 64
void ProcessSelection(int xPos, int yPos)
  {
  GLfloat fAspect;  // Screen aspect ratio

  // Space for selection buffer
  GLuint selectBuff[BUFFER_LENGTH];

  // Hit counter and viewport storeage
  GLint hits, viewport[4];

  // Setup selection buffer
  glSelectBuffer(BUFFER_LENGTH, selectBuff);
  
  // Get the viewport
  glGetIntegerv(GL_VIEWPORT, viewport);

  // Switch to projection and save the matrix
  glMatrixMode(GL_PROJECTION);
  glPushMatrix();

  // Change render mode
  glRenderMode(GL_SELECT);


  glLoadIdentity();
  gluPickMatrix(xPos, viewport[3] - yPos, 4,4, viewport);

  
  fAspect = (float)viewport[2] / (float)viewport[3];
  gluPerspective(45.0f, fAspect, 1.0, 425.0);

  // Chiama la funzione di rendering in modalita' selezione (la scena non viene disegnata).
        // display() deve contenere la chiamata a glMatrixMode(GL_MODELVIEW); all'inizio
        // perche' in questo punto in cui viene chiamata siamo in modo GL_PROJECTION.
        display();

  // Restore the projection matrix
  glMatrixMode(GL_PROJECTION);
  glPopMatrix();

  // Go back to modelview for normal rendering
  glMatrixMode(GL_MODELVIEW);

  // Collect the hits
  hits = glRenderMode(GL_RENDER);

  printf("# hits=%d\n",hits);
  if(hits >= 1)
    ProcessScene(selectBuff);
  }


void MouseCallback(int button, int state, int x, int y)
  {
  if(button == GLUT_LEFT_BUTTON && state == GLUT_DOWN)
    ProcessSelection(x, y);
}

int main(int argc, char** argv) {
   glutInit(&argc, argv);
   glutInitDisplayMode (GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
   glutInitWindowSize (800, 800);
   glutCreateWindow ("6. Ingranaggi");
   init ();
   glutReshapeFunc (reshape);
   glutDisplayFunc(display);
   glutIdleFunc(idle);
   glutKeyboardFunc (keyboard);
   glutMouseFunc(MouseCallback);
   glutMainLoop();
   return 0; 
}
