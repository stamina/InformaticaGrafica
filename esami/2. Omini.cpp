#include<GL/glut.h>
#include<GL/glu.h>
#include<GL/gl.h>
#include<cmath>


GLfloat globalAmbientLight[] = { 0.8f, 0.8f, 0.8f, 1.0f };
GLfloat componentAmbient0[] = { 0.6f, 0.6f, 0.6f, 1.0f };
GLfloat componentDiffuse0[] = { 0.7f, 0.7f, 0.7f, 1.0f };
GLfloat componentSpecular0[] = { 1.0f, 1.0f, 1.0f, 1.0f };
GLfloat posiz0[] = { 1.0f, 0.0f, 1.0f, 0.0f };
GLfloat  specref[] =  { 0.4, 0.4f, 0.4f, 1.0f }; // la potenza della luce: valori alti più bianco

GLfloat rotX = 0.0f;
GLfloat rotY = 0.0f;
GLfloat distrad = 10.0f;
GLint numO = 8;
bool nebb = false;


#define OMINO 1


void setupRC(){ // init
    // Grayish background
    glClearColor(0,0,0,1);
   
    // Cull backs of polygons
    glCullFace(GL_BACK); // rimuove superfici dietro?
    glEnable(GL_CULL_FACE);
    glEnable(GL_DEPTH_TEST);


    glEnable(GL_LIGHTING);
    glLightModelfv(GL_LIGHT_MODEL_AMBIENT,globalAmbientLight);
    
    glEnable(GL_COLOR_MATERIAL);
    glColorMaterial(GL_FRONT, GL_AMBIENT_AND_DIFFUSE);
    glMateriali(GL_FRONT, GL_SHININESS, 128);
    glEnable(GL_NORMALIZE);

    glEnable(GL_LIGHT0);
    glLightfv(GL_LIGHT0,GL_AMBIENT,componentAmbient0);
    glLightfv(GL_LIGHT0,GL_DIFFUSE,componentDiffuse0);
    glLightfv(GL_LIGHT0,GL_SPECULAR,componentSpecular0);
    glLightfv(GL_LIGHT0,GL_POSITION,posiz0);

  glEnable(GL_COLOR_MATERIAL);
   glColorMaterial(GL_FRONT, GL_AMBIENT_AND_DIFFUSE);
   glMaterialfv(GL_FRONT, GL_SPECULAR,specref); // luce riflessa bianca
   glMateriali(GL_FRONT, GL_SHININESS,30);

    // la lista la mettiamo in init
    glNewList(OMINO,GL_COMPILE);
        glColor3f (0.3, 0, 0);
        glutSolidSphere(1,50,50);

        // buttaci dei triangoli simpatici!
        glColor3f (0, 1, 0);
        glBegin(GL_TRIANGLES);
        glVertex3f(0,-1,-1); // v1
        glVertex3f(-sqrt(3)/2,-1,1.0/2); // v2
        glVertex3f(sqrt(3)/2,-1,1.0/2); // v3
        glEnd();

        glBegin(GL_TRIANGLES);
        glVertex3f(0,-4,-2); // v4
        glVertex3f(sqrt(3),-4,1.0); // v5
        glVertex3f(-sqrt(3),-4,1.0); // v6
        glEnd();

        glColor3f (0, 1, 1);
        glBegin(GL_QUADS); // v2 v6 v5 v3
        glVertex3f(-sqrt(3)/2,-1,1.0/2); // v2
        glVertex3f(-sqrt(3),-4,1.0); // v6
        glVertex3f(sqrt(3),-4,1.0); // v5
        glVertex3f(sqrt(3)/2,-1,1.0/2); // v3
        glEnd();

        glColor3f (1, 1, 0);
        glBegin(GL_QUADS); // v3 v5 v1 v4
        glVertex3f(sqrt(3)/2,-1,1.0/2); // v3
        glVertex3f(sqrt(3),-4,1.0); // v5
        glVertex3f(0,-4,-2); // v4
        glVertex3f(0,-1,-1); // v1
        glEnd();

        glColor3f (1, 0, 1);
        glBegin(GL_QUADS); // v1 v4 v6 v2
        glVertex3f(0,-1,-1); // v1
        glVertex3f(0,-4,-2); // v4
        glVertex3f(-sqrt(3),-4,1.0); // v6
        glVertex3f(-sqrt(3)/2,-1,1.0/2); // v2
        glEnd();        

    glEndList();

}

void nebbia(){
    if (nebb)
        nebb = false;
    else nebb = true;
}

void drawWorld(){
    GLfloat raggio = 0;
    for (GLint i = 0; i < numO; i++){
        raggio = (360.0 / numO)* i;
        glPushMatrix();
            glRotatef(raggio,0,1,0);
            glTranslatef(distrad,0,0);
            glRotatef(-90,0,1,0);
            glCallList(OMINO);
        glPopMatrix();
    }
}

void renderScene(void) //display 
{
    // Clear the window with current clearing color
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glShadeModel(GL_SMOOTH);
    glPushMatrix();  
        glRotatef(rotY,0,1,0);
        glRotatef(rotX,1,0,0);
    	drawWorld();
    glPopMatrix();


     GLfloat fogColor[4] = {0.5, 0.5, 0.5, 1.0};

      glFogi (GL_FOG_MODE, GL_LINEAR);
      glFogfv (GL_FOG_COLOR, fogColor);
      glFogf (GL_FOG_DENSITY, 5);
      glHint (GL_FOG_HINT, GL_DONT_CARE);
      glFogf (GL_FOG_START, 40);
      glFogf (GL_FOG_END, 60);

    if (nebb == true)
        glEnable(GL_FOG);
    else
        glDisable(GL_FOG);

    glutSwapBuffers();
}

void keyboard(unsigned char key, int x, int y){
   switch (key) {
      case 27:
         exit(0);
         break;
      case 'z':
         rotY-=2;
         break;
      case 'x':
         rotY+=2;
         break;
    case 'v':
         rotX-= 2;
        break;
    case 'c':
         rotX+= 2;
        break;
    case '+':
         distrad+=0.1;
         break;
    case '-':
         distrad-=0.1;
         break;
    case 'w':
         numO += 1;
         if (numO > 12) numO = 12;
         break;
    case 's':
         numO -= 1;
         if (numO < 2) numO = 2;
         break;
    case 'n':
        nebbia();
        break;
   }
   glutPostRedisplay();
}



void changeSize(int w, int h) {
    GLfloat fAspect;

    if(h == 0)
        h = 1;

    glViewport(0, 0, w, h);
        
    fAspect = (GLfloat)w / (GLfloat)h;

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
	
    // Set the clipping volume
    gluPerspective(45.0f, fAspect, 1.0f, 800.0f);
        
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();  
    glTranslatef(0.0f, 0.0f, -40.0f);
    }

void idleScene(){
	 glutPostRedisplay();
}

int main(int argc, char* argv[]) {
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
    glutInitWindowSize(800,600);
    glutCreateWindow("OpenGL Blending and Transparency");
    glutReshapeFunc(changeSize);
    glutDisplayFunc(renderScene);
    glutKeyboardFunc(keyboard);
    // glutSpecialFunc(SpecialKeys);
    glutIdleFunc(idleScene);
    
    setupRC();
    glutMainLoop();

    return 0;
}
